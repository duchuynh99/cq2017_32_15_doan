package com.ygaps.travelapp.Recorder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ygaps.travelapp.R;

import java.io.IOException;
import java.util.UUID;

public class RecorderActivity extends AppCompatActivity {

    String pathSave = "";
    MediaRecorder mediaRecorder;
    MediaPlayer mediaPlayer;

    ImageButton imbRecord;
    Button btnPlayAudio;
    boolean isRecording = false;
    final int REQUEST_PERMISSION_CODE =1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorder);
        imbRecord = findViewById(R.id.imbRecord);
        btnPlayAudio = findViewById(R.id.btnPlayAudio);

                imbRecord.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkPermissionRecord()) {
                            if (!isRecording) {
                                isRecording = true;
                                imbRecord.setImageResource(R.drawable.ic_microphone_playing);
                                pathSave = Environment.getExternalStorageDirectory().getAbsolutePath()
                                        + "/"
                                        + UUID.randomUUID().toString() + "_audio_record.3gp";
                                setupMediaRecorder();
                                try {
                                    mediaRecorder.prepare();
                                    mediaRecorder.start();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                isRecording = false;
                                imbRecord.setImageResource(R.drawable.ic_microphone_off);
                                mediaRecorder.stop();


                            }

                        } else {
                            requestPermissionRecord();
                        }
                    }
                });
                btnPlayAudio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mediaPlayer = new MediaPlayer();
                        try {
                            mediaPlayer.setDataSource(pathSave);
                            mediaPlayer.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        mediaPlayer.start();
                        Toast.makeText(getApplicationContext(),"Playing",Toast.LENGTH_SHORT).show();
                    }
                });


    }

    private void setupMediaRecorder() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(pathSave);
    }

    private void requestPermissionRecord(){
        ActivityCompat.requestPermissions(this, new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO
        },REQUEST_PERMISSION_CODE);
    }
    private boolean checkPermissionRecord(){
        int write_external_storage_result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int record_audio_result = ContextCompat.checkSelfPermission(this,Manifest.permission.RECORD_AUDIO);
        return write_external_storage_result == PackageManager.PERMISSION_GRANTED &&
                record_audio_result == PackageManager.PERMISSION_GRANTED;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case REQUEST_PERMISSION_CODE:
            {
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this,"Permission Granted!",Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this,"Permission Denied!",Toast.LENGTH_SHORT).show();

                }
            }
                break;
        }
    }
}

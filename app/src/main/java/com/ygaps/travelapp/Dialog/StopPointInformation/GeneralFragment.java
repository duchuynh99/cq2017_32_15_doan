package com.ygaps.travelapp.Dialog.StopPointInformation;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ygaps.travelapp.Model.PointStar;
import com.ygaps.travelapp.Model.PointStarAPI;
import com.ygaps.travelapp.Model.Province;
import com.ygaps.travelapp.Model.ServiceType;
import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneralFragment extends Fragment {
    private StopPoint stopPoint;
    private Context mContext;
    private List<ServiceType> serviceList;
    private List<Province> provinceList;

    public GeneralFragment(StopPoint stopPoint, Context mContext) {
        this.stopPoint = stopPoint;
        this.mContext = mContext;
        serviceList = dataForListServiceType();
        provinceList = dataForListProvince();
    }

    private ArrayList<PointStar> mListPointStar;
    private TextView tvName, tvServiceType, tvAddress, tvProvince, tvCost, tvArriveAt, tvLeaveAt, tvRatingAverage;
    private EditText etStopPointReview;
    private Button btSubmitReview;
    private RatingBar rbEditRating, rbRatingAverage;
    private ProgressBar pdReviewStar1, pdReviewStar2, pdReviewStar3, pdReviewStar4, pdReviewStar5;
    String TOKEN = MyAPIClient.getInstance().getAccessToken();
//    private String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoidGhpbmg5NyIsImVtYWlsIjoibmd1eWVubWluaHRoaW5oOTdAZ21haWwuY29tIiwiZXhwIjoxNTUzODczNDU1NTY2LCJpYXQiOjE1NTEyODE0NTV9.lQ-RkLSwD3UyRXWvSRaTIsn1f_3ZRMWd-nfRutcwXFw";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general_stop_point_info, container, false);
        addControl(view);

        tvName.setText(stopPoint.getName());
        tvServiceType.setText(serviceList.get(Integer.parseInt(stopPoint.getServiceTypeId().toString())).getName());
        tvAddress.setText(stopPoint.getAddress());
        tvProvince.setText(provinceList.get(Integer.parseInt(stopPoint.getProvinceId().toString())).getName());
        tvCost.setText(priceRangeFormatter(stopPoint));
        tvArriveAt.setText(calendarFormatter(stopPoint));
        tvLeaveAt.setText(calendarFormatter(stopPoint));
        btSubmitReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etStopPointReview.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, getString(R.string.review_submit_warning), Toast.LENGTH_SHORT).show();
                } else
                    submitPointReview();
            }
        });

        fetchRatings(TOKEN, stopPoint.getId());

        return view;
    }

    private void fetchRatings(String token, Number id) {
        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.getFeedbackPointStar(token, id).enqueue(new Callback<PointStarAPI>() {
            @Override
            public void onResponse(Call<PointStarAPI> call, Response<PointStarAPI> response) {
                if (response.isSuccessful()) {
                    mListPointStar = new ArrayList<>(response.body().getPointStats());
                    float averageRating = AverageRating(mListPointStar);
                    tvRatingAverage.setText(String.valueOf(averageRating));
                    rbRatingAverage.setRating(averageRating);
                    pdReviewStar1.setProgress(mListPointStar.get(0).getTotal());
                    pdReviewStar2.setProgress(mListPointStar.get(1).getTotal());
                    pdReviewStar3.setProgress(mListPointStar.get(2).getTotal());
                    pdReviewStar4.setProgress(mListPointStar.get(3).getTotal());
                    pdReviewStar5.setProgress(mListPointStar.get(4).getTotal());
                } else {
                    Log.d("API_Call", "[General Fragment] Error ");
                }
            }

            @Override
            public void onFailure(Call<PointStarAPI> call, Throwable t) {
                Log.d("API_Call", "[General Fragment] Can not connect to Server");
            }
        });
    }

    private void submitPointReview() {
        JsonObject body = new JsonObject();
        body.addProperty("serviceId", stopPoint.getId());
        body.addProperty("point", rbEditRating.getRating());
        body.addProperty("feedback", etStopPointReview.getText().toString());

        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.addPointReview(TOKEN, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    Toast.makeText(mContext, getString(R.string.review_submited), Toast.LENGTH_SHORT).show();
                    etStopPointReview.getText().clear();
                    etStopPointReview.clearFocus();
                } else
                    Toast.makeText(mContext, getString(R.string.review_submit_error).concat(" " + response.code()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        });

    }

    //Helpers------------------------------------------------------------------------------------------------------------
    private void addControl(View view) {
        tvName = view.findViewById(R.id.tvName_StopPointDetails);
        tvServiceType = view.findViewById(R.id.tvServiceType_StopPointDetails);
        tvAddress = view.findViewById(R.id.tvAddress_StopPointDetails);
        tvProvince = view.findViewById(R.id.tvProvince_StopPointDetails);
        tvCost = view.findViewById(R.id.tvCost_StopPointDetails);
        tvCost = view.findViewById(R.id.tvCost_StopPointDetails);
        tvArriveAt = view.findViewById(R.id.tvArrive_StopPointDetails);
        tvLeaveAt = view.findViewById(R.id.tvLeave_StopPointDetails);
        etStopPointReview = view.findViewById(R.id.etStopPointReview);
        btSubmitReview = view.findViewById(R.id.btSubmitReview_StopPoint);
        rbEditRating = view.findViewById(R.id.rbEditRating_StopPoint);
        tvRatingAverage = view.findViewById(R.id.tvRatingAverage_StopPoint);
        rbRatingAverage = view.findViewById(R.id.rbRatingAverage_StopPoint);
        pdReviewStar1 = view.findViewById(R.id.pdReviewStar1_StopPoint);
        pdReviewStar2 = view.findViewById(R.id.pdReviewStar2_StopPoint);
        pdReviewStar3 = view.findViewById(R.id.pdReviewStar3_StopPoint);
        pdReviewStar4 = view.findViewById(R.id.pdReviewStar4_StopPoint);
        pdReviewStar5 = view.findViewById(R.id.pdReviewStar5_StopPoint);
    }

    //Data calculator----------------------------------------------------------------------------------------------------
    private float AverageRating(ArrayList<PointStar> mListPointStar) {
        int sum = 0, numStars = 0;
        for (PointStar pointStar : mListPointStar) {
            sum += pointStar.getPoint() * pointStar.getTotal();
            numStars += pointStar.getTotal();
        }
        return numStars == 0 ? 0 : sum / numStars;
    }

    private List<ServiceType> dataForListServiceType() {
        ArrayList<ServiceType> listServiceType = new ArrayList<>();
        BufferedReader reader;
        String path_file = "service_type.csv";
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open(path_file)));
            String line = reader.readLine();
            int flag = 0;
            while (line != null) {
                if (flag > 0) {
                    String[] s = line.split(",");
                    listServiceType.add(new ServiceType(Integer.parseInt(s[0]), s[1]));
                }
                line = reader.readLine();
                flag++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listServiceType;
    }

    private List<Province> dataForListProvince() {
        ArrayList<Province> listProvince = new ArrayList<>();
        BufferedReader reader;
        String path_file = "province.csv";
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open(path_file)));
            String line = reader.readLine();
            int flag = 0;
            while (line != null) {
                if (flag > 0) {
                    String[] s = line.split(",");
                    listProvince.add(new Province(Integer.parseInt(s[0]), s[1], s[2]));
                }
                line = reader.readLine();
                flag++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listProvince;
    }

    //Data formatter-----------------------------------------------------------------------------------------------------
    private String priceRangeFormatter(StopPoint stopPoint) {
        String result = null;
        if (stopPoint.getMinCost().intValue() != 0) {
            result = stopPoint.getMinCost().toString();
        }
        if (stopPoint.getMaxCost().intValue() != 0) {
            if (result == null) {
                result = stopPoint.getMaxCost().toString() + " VNĐ";
            } else {
                result += " - " + stopPoint.getMaxCost().toString() + " VNĐ";
            }
        }
        if (result == null) {
            result = "FREE";
        }
        return result;
    }

    private String calendarFormatter(StopPoint stopPoint) {
        String result = null;
        Long start, end;
        Calendar cal1, cal2;
        String pattern = "HH:mm     dd/MM/yyyy";//dd/MM/YYYY
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        start = Long.parseLong(stopPoint.getArrivalAt().toString());
        end = Long.parseLong(stopPoint.getLeaveAt().toString());
        cal1 = cal2 = Calendar.getInstance();

        if (start != null) {
            cal1.setTimeInMillis(start);
            result = formatter.format(cal1.getTime());
        }
        if (end != null && !(end.equals(start))) {
            cal2.setTimeInMillis(end);
            if (result == null) {
                result = formatter.format(cal2.getTime());
            } else {
                result += " - " + formatter.format(cal2.getTime());
            }
        }
        if (result == null) {
            result = "No calendar";
        }
        return result;
    }
}
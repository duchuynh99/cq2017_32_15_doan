package com.ygaps.travelapp.Dialog.StopPointInformation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Model.Feedback;
import com.ygaps.travelapp.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class PointReviewsAdapter extends RecyclerView.Adapter<PointReviewsAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Feedback> feedbacks;

    public PointReviewsAdapter(Context mContext, ArrayList<Feedback> mReviews) {
        this.mContext = mContext;
        this.feedbacks = mReviews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.feedback_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Feedback feedback = feedbacks.get(position);

        String url = "https://img.thuthuatphanmem.vn/uploads/2018/09/19/avatar-facebook-chat-1_105603552.gif";
        Picasso.get().load(url).fit().centerCrop().into(holder.ivAvatar);
        holder.tvName.setText(feedback.getName());
        holder.tvDateCreated.setText(calendarFormatter(feedback));
        holder.tvPhone.setText(feedback.getPhone());
        holder.tvReview.setText(feedback.getFeedback());
        holder.rbReviewRating.setRating(feedback.getPoint());
    }

    @Override
    public int getItemCount() {
        return feedbacks.size();
    }

    public void updateData(ArrayList<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
        notifyDataSetChanged();
    }

    //Data formatter-----------------------------------------------------------------------------------------------------
    private String calendarFormatter(Feedback feedback) {
        long start = Long.parseLong(feedback.getCreatedOn().toString());
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(start);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(cal1.getTime());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivAvatar;
        TextView tvName, tvDateCreated, tvReview, tvPhone;
        RatingBar rbReviewRating;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.ivAvatar_PointReview);
            tvName = itemView.findViewById(R.id.tvName_PointReview);
            tvDateCreated = itemView.findViewById(R.id.tvDateCreated_PointReview);
            tvReview = itemView.findViewById(R.id.tvPointReview);
            tvPhone = itemView.findViewById(R.id.tvPhoneNumber_PointReview);
            rbReviewRating = itemView.findViewById(R.id.rbPointReview);
        }
    }
}
package com.ygaps.travelapp.Dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Dialog.StopPointInformation.StopPointInformationPopup;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.Model.StopPoint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StopPointAdapter extends RecyclerView.Adapter<StopPointAdapter.StopPointHolder> {
    ArrayList<StopPoint> listStopPoint;
    Context mContext;
    FragmentManager fm;

    public StopPointAdapter(ArrayList<StopPoint> listStopPoint, Context context, FragmentManager fm) {
        this.listStopPoint = listStopPoint;
        this.mContext = context;
        this.fm = fm;
    }

    @NonNull
    @Override
    public StopPointHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_list_stop_point,viewGroup,false);
        StopPointHolder viewHolder = new StopPointHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final StopPointHolder holder, final int i) {
        try {
            final StopPoint item = listStopPoint.get(i);
            holder.tvNameSP.setText(item.getName());
            holder.tvAddress.setText(item.getAddress());
            Date arrivedDate = new Date(item.getArrivalAt().longValue());
            Date leftDate = new Date(item.getLeaveAt().longValue());
            SimpleDateFormat formater = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            StringBuilder builder = new StringBuilder();
            builder.append(formater.format(arrivedDate));
            builder.append(" - ");
            builder.append(formater.format(leftDate));
            holder.tvTime.setText(builder.toString());
            holder.imbRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listStopPoint.remove(i);
                    notifyDataSetChanged();
                }
            });
            //Khởi tạo popup thông tin điểm dừng
            final StopPointInformationPopup popup = new StopPointInformationPopup(mContext, item);
            holder.lnItemStopPoint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StopPointPopup popup = new StopPointPopup(mContext.getApplicationContext(), item, "", null, listStopPoint);
                    popup.show(fm, popup.getTag());
                    fm.executePendingTransactions();
                }
            });
        }
        catch(Exception ex)
        {

        }
        //Xử lí text time
    }

    @Override
    public int getItemCount() {
        return listStopPoint.size();
    }

    public class StopPointHolder extends RecyclerView.ViewHolder{
        TextView tvNameSP,tvAddress,tvTime;
        ImageButton imbRemove;
        LinearLayout lnItemStopPoint;
        public StopPointHolder(@NonNull View itemView) {
            super(itemView);
            tvNameSP = itemView.findViewById(R.id.tvNameSP);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            tvTime = itemView.findViewById(R.id.tvTime);
            lnItemStopPoint = itemView.findViewById(R.id.lnItemStopPoint);
            imbRemove = itemView.findViewById(R.id.imbRemove);
        }
    }
}

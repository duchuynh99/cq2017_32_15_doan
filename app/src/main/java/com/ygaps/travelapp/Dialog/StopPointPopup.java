package com.ygaps.travelapp.Dialog;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.ygaps.travelapp.Model.Province;
import com.ygaps.travelapp.Model.ServiceType;
import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class StopPointPopup extends DialogFragment {
    Context mContext;
    Dialog dialog;
    StopPoint mStopPoint;
    List<StopPoint> listStopPoint;
    View view;
    ImageButton imbClose;
    Button btnAddStopPoint;
    EditText edtStopPointName, edtAddress;
    Spinner spinnerServiceType, spinnerProvince;
    EditText edtMinCost, edtMaxCost;
    Button btnTimeArrive, btnDateArrive, btnTimeLeave, btnDateLeave;
    String tourID;
    String province;
    boolean isAddedStopPoint = false;

    public StopPointPopup(Context context, StopPoint stopPoint, String tourID, String province, List<StopPoint> list) {
        this.mContext = context;
        this.mStopPoint = stopPoint;
        this.tourID = tourID;
        this.province = province;
        this.listStopPoint = list;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        try {
            view = View.inflate(getContext(), R.layout.popup_stop_point, null);
            imbClose = view.findViewById(R.id.imbClose);
            edtStopPointName = view.findViewById(R.id.edtStopPointName);
            spinnerServiceType = view.findViewById(R.id.spinnerServiceType);
            spinnerProvince = view.findViewById(R.id.spinnerProvince);
            edtAddress = view.findViewById(R.id.edtAddress);
            edtMaxCost = view.findViewById(R.id.edtMaxCost);
            edtMinCost = view.findViewById(R.id.edtMinCost);
            btnDateArrive = view.findViewById(R.id.btnDateArrive);
            btnDateLeave = view.findViewById(R.id.btnDateLeave);
            btnTimeArrive = view.findViewById(R.id.btnTimeArrive);
            btnTimeLeave = view.findViewById(R.id.btnTimeLeave);
            btnAddStopPoint = view.findViewById(R.id.btnAddStopPoint);
            if(listStopPoint.contains(mStopPoint))
                btnAddStopPoint.setText(getString(R.string.apply_button));
            else btnAddStopPoint.setText(getString(R.string.add_button));
            edtAddress.setText(mStopPoint.getAddress());
            imbClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
            btnAddStopPoint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(edtStopPointName.getText().toString().length() <= 0)
                    {
                        edtStopPointName.setError("Stop point must have name"); return;
                    }
                    if(spinnerProvince.getSelectedItemId() == 0) {
                        Toast.makeText(getContext(), "You must select a province", Toast.LENGTH_SHORT).show(); return;
                    }
                    if(spinnerServiceType.getSelectedItemId() == 0) {
                        Toast.makeText(getContext(), "You must select a service type", Toast.LENGTH_SHORT).show(); return;
                    }
                    if(edtAddress.getText().toString().length() <= 0) {
                        edtAddress.setError("Stop point must have address"); return;
                    }
                    if(btnTimeArrive.getText().toString().equals("Time")) {
                        btnTimeArrive.setError("Stop point must have arrived time"); return;
                    }
                    if(btnDateArrive.getText().toString().equals("Select date")) {
                        btnDateArrive.setError("Stop point must have arrived date"); return;
                    }
                    if(btnTimeLeave.getText().toString().equals("Time")) {
                        btnTimeArrive.setError("Stop point must have left time"); return;
                    }
                    if(btnDateLeave.getText().toString().equals("Select date")) {
                        btnTimeArrive.setError("Stop point must have left date"); return;
                    }
                    mStopPoint.setName(edtStopPointName.getText().toString());
                    mStopPoint.setServiceTypeId(spinnerServiceType.getSelectedItemId());
                    mStopPoint.setProvinceId(spinnerProvince.getSelectedItemId());
                    mStopPoint.setAddress(edtAddress.getText().toString());
                    mStopPoint.setMinCost(Integer.parseInt(edtMinCost.getText().toString()));
                    mStopPoint.setMaxCost(Integer.parseInt(edtMaxCost.getText().toString()));
                    String dateTimeArrive = btnDateArrive.getText().toString() + " " + btnTimeArrive.getText().toString();
                    try {
                        Date arrive = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(dateTimeArrive);
                        mStopPoint.setArrivalAt(arrive.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    String dateTimeLeave = btnDateLeave.getText().toString() + " " + btnTimeLeave.getText().toString();
                    try {
                        Date arrive = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(dateTimeLeave);
                        mStopPoint.setLeaveAt(arrive.getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(!listStopPoint.contains(mStopPoint))
                        listStopPoint.add(mStopPoint);
                    dialog.dismiss();

                }
            });
            addEventSpinnerServiceType(spinnerServiceType);
            addEventSpinnerProvince(spinnerProvince);
            addTime(btnTimeArrive);
            addTime(btnTimeLeave);
            addDate(btnDateArrive);
            addDate(btnDateLeave);
            dialog.setContentView(view);
            if (mStopPoint.getProvinceId() != null)
                spinnerProvince.setSelection(mStopPoint.getProvinceId().intValue());
            if (mStopPoint.getServiceTypeId() != null)
                spinnerServiceType.setSelection(mStopPoint.getServiceTypeId().intValue());
            if (mStopPoint.getMinCost() != null)
                edtMinCost.setText(mStopPoint.getMinCost().toString());
            if (mStopPoint.getMaxCost() != null)
                edtMaxCost.setText(mStopPoint.getMaxCost().toString());
            if (mStopPoint.getName() != null)
                edtStopPointName.setText(mStopPoint.getName());
            Date arrivedDate = new Date(mStopPoint.getArrivalAt().longValue());
            Date leftDate = new Date(mStopPoint.getLeaveAt().longValue());
            SimpleDateFormat timeFormater = new SimpleDateFormat("HH:mm");
            SimpleDateFormat dateFormater = new SimpleDateFormat("dd/MM/yyyy");
            btnTimeArrive.setText(timeFormater.format(arrivedDate));
            btnDateArrive.setText(dateFormater.format(arrivedDate));
            btnTimeLeave.setText(timeFormater.format(leftDate));
            btnDateLeave.setText(dateFormater.format(leftDate));

        }
        catch(Exception ex)
        {

        }
    }
    public boolean getIsAddedStopPoint(){
        return isAddedStopPoint;
    }
    private void addDate(final Button button) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                button.setText(date);
                /*calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);*/
                //newTour.setStartDate(calendar.getTimeInMillis());
            }
        }, year, month, day);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();

            }
        });
    }

    private void addTime(final Button button) {
        Calendar calendar = Calendar.getInstance();
        int mHour = calendar.get(Calendar.HOUR_OF_DAY);
        int mMin = calendar.get(Calendar.MINUTE);
        final TimePickerDialog timePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int min) {
                String time = "";
                if (hour < 10)
                    time += "0";
                time += hour + ":";
                if (min < 10)
                    time += "0";
                time += min;
                button.setText(time);
            }
        }, mHour, mMin, false);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePicker.show();

            }
        });
    }

    private void addEventSpinnerServiceType(Spinner spinner) {
        final List<ServiceType> data = dataForListServiceType();
        List<String> listServiceType = new ArrayList<>();
        for (ServiceType item : data) {
            listServiceType.add(item.getName());
        }
        listServiceType.add(0, "--Choose service type--");
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, listServiceType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void addEventSpinnerProvince(Spinner spinner) {
        final List<Province> data = dataForListProvince();
        List<String> list = new ArrayList<>();
        for (Province item : data) {
            list.add(item.getName());
        }
        Collections.sort(list); //sắp xếp tăng dần theo a-z
        list.add(0, "--Choose province--");
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        int index = adapter.getPosition(province);
        if(index > 0) spinner.setSelection(index);
    }

    private List<ServiceType> dataForListServiceType() {
        ArrayList<ServiceType> listServiceType = new ArrayList<>();
        BufferedReader reader;
        String path_file = "service_type.csv";
        try {
            reader = new BufferedReader(new InputStreamReader(getContext().getAssets().open(path_file)));
            String line = reader.readLine();
            int flag = 0;
            while (line != null) {
                if (flag > 0) {
                    String[] s = line.split(",");
                    listServiceType.add(new ServiceType(Integer.parseInt(s[0]), s[1]));
                }
                line = reader.readLine();
                flag++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listServiceType;
    }

    private List<Province> dataForListProvince() {
        ArrayList<Province> listProvince = new ArrayList<>();
        BufferedReader reader;
        String path_file = "province.csv";
        try {
            reader = new BufferedReader(new InputStreamReader(getContext().getAssets().open(path_file)));
            String line = reader.readLine();
            int flag = 0;
            while (line != null) {
                if (flag > 0) {
                    String[] s = line.split(",");
                    listProvince.add(new Province(Integer.parseInt(s[0]), s[1], s[2]));
                }
                line = reader.readLine();
                flag++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listProvince;
    }
}

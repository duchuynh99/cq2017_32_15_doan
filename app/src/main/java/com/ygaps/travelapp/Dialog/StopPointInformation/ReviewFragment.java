package com.ygaps.travelapp.Dialog.StopPointInformation;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ygaps.travelapp.Model.Feedback;
import com.ygaps.travelapp.Model.FeedbackAPI;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewFragment extends Fragment {

    private Number PointId;
    RecyclerView rvReviews;
    PointReviewsAdapter adapterReviews;
    SwipeRefreshLayout refreshLayout;

    String TOKEN = MyAPIClient.getInstance().getAccessToken();
//    String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoidGhpbmg5NyIsImVtYWlsIjoibmd1eWVubWluaHRoaW5oOTdAZ21haWwuY29tIiwiZXhwIjoxNTUzODczNDU1NTY2LCJpYXQiOjE1NTEyODE0NTV9.lQ-RkLSwD3UyRXWvSRaTIsn1f_3ZRMWd-nfRutcwXFw";

    public ReviewFragment(Number pointId) {
        PointId = pointId;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        addControl(view);
        refreshLayout.setRefreshing(true);
        rvReviews.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterReviews = new PointReviewsAdapter(getContext(), new ArrayList<Feedback>());
        rvReviews.setAdapter(adapterReviews);
        rvReviews.setHasFixedSize(true);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchReviews(PointId);
            }
        });
        fetchReviews(PointId);
        return view;
    }

    private void fetchReviews(Number PointId) {
        UserService mUserService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mUserService.getStopPointComments(TOKEN, PointId, 1, 225).enqueue(new Callback<FeedbackAPI>() {
            @Override
            public void onResponse(Call<FeedbackAPI> call, Response<FeedbackAPI> response) {
                if (response.isSuccessful()) {
                    refreshLayout.setRefreshing(false);
                    adapterReviews.updateData(response.body().getFeedbackList());
                } else {
                    Log.d("Reviews fragment", "Error Code " + response.code());
                }
            }

            @Override
            public void onFailure(Call<FeedbackAPI> call, Throwable t) {
                //Hiển thị lỗi.
                Log.d("Reviews fragment", "error loading reviews from API");
            }
        });
    }

    //Helpers------------------------------------------------------------------------------------------------------------
    private void addControl(View view) {
        rvReviews = view.findViewById(R.id.rvReviews);
        refreshLayout = view.findViewById(R.id.srlSwipeToRefress);
    }
}
package com.ygaps.travelapp.Dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ygaps.travelapp.Model.UserInfo;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvitationMemberPopup extends DialogFragment {
    Context mContext;
    View view;

    ImageButton imbClose;
    EditText edtKeyword;
    RecyclerView rvListMember;
    Button btnSearch;
    UserService userService;
    String tourID;
    public InvitationMemberPopup(Context mContext, String tourID) {
        this.mContext = mContext;
        this.tourID = tourID;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        view = View.inflate(getContext(), R.layout.popup_invite_member,null);

        imbClose = view.findViewById(R.id.imbClose);
        rvListMember = view.findViewById(R.id.rvListMember);
        edtKeyword = view.findViewById(R.id.edtKeyword);
        btnSearch = view.findViewById(R.id.btnSearch);
        imbClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        rvListMember.setLayoutManager(layoutManager);
        rvListMember.addItemDecoration(new DividerItemDecoration(mContext,0)); //orientation 0 để xóa vạch ngang giữa 2 item, phía trên thì có.

        userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        JsonObject request = new JsonObject();
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"Loading...",Toast.LENGTH_SHORT).show();
               ArrayList<UserInfo> listSearch = new ArrayList<>();
               String keyword= edtKeyword.getText().toString().toLowerCase();
                Call<JsonObject> call = userService.searchUser(keyword,1,10);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if(response.code()==200){
                            JsonArray listUser = response.body().getAsJsonArray("users");
                            for(int i=0 ; i< listUser.size();i++){
                                Gson gson = new Gson();
                                UserInfo item = gson.fromJson(listUser.get(i),UserInfo.class);
                                listSearch.add(item);
                            }
                            if(listSearch.size()==0)
                                Toast.makeText(getContext(),"Không tìm thấy!",Toast.LENGTH_SHORT).show();
                            InvitationMemberAdapter adapter = new InvitationMemberAdapter(getContext(),listSearch,tourID);
                            rvListMember.setAdapter(adapter);
                        }
                        else
                            Toast.makeText(getContext(),"Server error",Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            }
        });
        //Xử lí adapter
        /*LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        rvListMember.setLayoutManager(layoutManager);
        rvListMember.addItemDecoration(new DividerItemDecoration(mContext,0)); //orientation 0 để xóa vạch ngang giữa 2 item, phía trên thì có.
        InvitationMemberAdapter adapter = new InvitationMemberAdapter();
        rvListMember.setAdapter(adapter);*/
        dialog.setContentView(view);

    }

}

package com.ygaps.travelapp.Dialog.StopPointInformation;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.R;
import com.google.android.material.tabs.TabLayout;

public class StopPointInformationPopup extends DialogFragment {
    private Context mContext;
    private StopPoint mStopPoint;
    ImageButton imbClose;
    TabLayout tabLayout;
    ViewPager viewPager;

    public StopPointInformationPopup(Context mContext, StopPoint mStopPoint) {
        this.mContext = mContext;
        this.mStopPoint = mStopPoint;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        dialog.setTitle(getString(R.string.stop_point_information_dialog_title));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        View view = inflater.inflate(R.layout.popup_stop_point_information, container, false);
        addControls(view);
        imbClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        CustomFragmentPagerAdapter mAdaptor = new CustomFragmentPagerAdapter(getChildFragmentManager());
        mAdaptor.addFragment(getString(R.string.stop_point_information_general_tab_title), new GeneralFragment(mStopPoint, mContext));
        mAdaptor.addFragment(getString(R.string.stop_point_information_reviews_tab_title), new ReviewFragment(mStopPoint.getId()));
        viewPager.setAdapter(mAdaptor);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    private void addControls(View view) {
        imbClose = (ImageButton) view.findViewById(R.id.imbClose1);
        tabLayout = (TabLayout) view.findViewById(R.id.tlStopPointInformation);
        viewPager = (ViewPager) view.findViewById(R.id.vpStopPointInformation);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
}

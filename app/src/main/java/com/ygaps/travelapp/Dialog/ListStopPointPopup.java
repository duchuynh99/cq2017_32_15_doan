package com.ygaps.travelapp.Dialog;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Model.Province;
import com.ygaps.travelapp.Model.ServiceType;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.Model.StopPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class ListStopPointPopup extends DialogFragment {
    Context mContext;
    Dialog dialog;
    View view;
    ImageButton imbClose;
    RecyclerView rvListStopPoint;
    ArrayList<StopPoint> listStopPoint;
    Button btnAddStopPoint;
    EditText edtStopPointName, edtAddress;
    Spinner spinnerServiceType, spinnerProvince;
    EditText edtMinCost, edtMaxCost;
    Button btnTimeArrive, btnDateArrive, btnTimeLeave,btnDateLeave;


    public ListStopPointPopup(Context context,ArrayList<StopPoint> listStopPoint) {
        this.mContext = context;
        this.listStopPoint = listStopPoint;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        view = View.inflate(getContext(), R.layout.popup_list_stop_point,null);

        imbClose = view.findViewById(R.id.imbClose);
        rvListStopPoint = view.findViewById(R.id.rvListStopPoint);

        imbClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        //Xử lí adapter
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        rvListStopPoint.setLayoutManager(layoutManager);
        rvListStopPoint.addItemDecoration(new DividerItemDecoration(mContext,0)); //orientation 0 để xóa vạch ngang giữa 2 item, phía trên thì có.
        StopPointAdapter adapter = new StopPointAdapter(listStopPoint,mContext, getFragmentManager());
        rvListStopPoint.setAdapter(adapter);
        dialog.setContentView(view);

    }

    private void addDate(final Button button){
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                button.setText(date);
                /*calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);*/
                //newTour.setStartDate(calendar.getTimeInMillis());
            }
        }, year, month, day);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();

            }
        });
    }
    private void addTime(final Button button){
        Calendar calendar = Calendar.getInstance();
        int mHour = calendar.get(Calendar.HOUR_OF_DAY);
        int mMin = calendar.get(Calendar.MINUTE);
        final TimePickerDialog timePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int min) {
                String time = hour + "H";
                if(min<10)
                    time+="0";
                time+=min+"'";
                button.setText(time);
            }
        },mHour,mMin,false);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timePicker.show();

            }
        });
    }
    private void addEventSpinnerServiceType(Spinner spinner){
        List<ServiceType> data = dataForListServiceType();
        List<String> listServiceType = new ArrayList<>();
        for(ServiceType item:data){
            listServiceType.add(item.getName());
        }
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,listServiceType);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private void addEventSpinnerProvince(Spinner spinner){
        List<Province> data = dataForListProvince();
        List<String> list = new ArrayList<>();
        for(Province item:data){
            list.add(item.getName());
        }
        Collections.sort(list); //sắp xếp tăng dần theo a-z
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private List<ServiceType> dataForListServiceType(){
        ArrayList<ServiceType> listServiceType = new ArrayList<>();
        BufferedReader reader;
        String path_file = "service_type.csv";
        try {
            reader = new BufferedReader(new InputStreamReader(getContext().getAssets().open(path_file)));
            String line = reader.readLine();
            int flag = 0;
            while (line != null) {
                if(flag>0){
                    String []s = line.split(",");
                    listServiceType.add(new ServiceType(Integer.parseInt(s[0]),s[1]));
                }
                line = reader.readLine();
                flag++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listServiceType;
    }
    private List<Province> dataForListProvince(){
        ArrayList<Province> listProvince = new ArrayList<>();
        BufferedReader reader;
        String path_file = "province.csv";
        try {
            reader = new BufferedReader(new InputStreamReader(getContext().getAssets().open(path_file)));
            String line = reader.readLine();
            int flag = 0;
            while (line != null) {
                if(flag>0){
                    String []s = line.split(",");
                    listProvince.add(new Province(Integer.parseInt(s[0]),s[1],s[2]));
                }
                line = reader.readLine();
                flag++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listProvince;
    }
}

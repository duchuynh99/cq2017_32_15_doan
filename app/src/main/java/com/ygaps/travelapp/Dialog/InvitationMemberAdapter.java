package com.ygaps.travelapp.Dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.ygaps.travelapp.Dialog.StopPointInformation.StopPointInformationPopup;
import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.Model.UserInfo;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvitationMemberAdapter extends RecyclerView.Adapter<InvitationMemberAdapter.InvitationHolder> {
    Context mContext;
    List<UserInfo> listUser;
    UserService userService;
    String mTourID;
    String token = MyAPIClient.getInstance().getAccessToken();
    public InvitationMemberAdapter(Context mContext, List<UserInfo> listUser, String mTourID) {
        this.mContext = mContext;
        this.listUser = listUser;
        this.mTourID = mTourID;
        userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
    }

    @NonNull
    @Override
    public InvitationMemberAdapter.InvitationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_member_invitation,viewGroup,false);
        InvitationHolder viewHolder = new InvitationHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull InvitationHolder holder, int position) {
        UserInfo item = listUser.get(position);
        holder.txtNameUser.setText(item.getFull_name());
        holder.txtAddress.setText(item.getAddress());

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("tourId",mTourID);
        jsonObject.addProperty("invitedUserId",item.getId());
        jsonObject.addProperty("isInvited",true);
        holder.btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<JsonObject> call = userService.addMember(token,jsonObject);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(mContext,"Đã mời",Toast.LENGTH_SHORT).show();
                            holder.btnInvite.setEnabled(false);
                            holder.btnInvite.setText("Đã mời");
                        }
                        else{
                            Toast.makeText(mContext,"Không thể mời!",Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            }
        });
    }


    @Override
    public int getItemCount() {
        return listUser.size();
    }

    public class InvitationHolder extends RecyclerView.ViewHolder{
        TextView txtNameUser, txtAddress;
        Button btnInvite;
        public InvitationHolder(@NonNull View itemView) {
            super(itemView);
            txtAddress = itemView.findViewById(R.id.txtAddressUser);
            txtNameUser = itemView.findViewById(R.id.txtNameUser);
            btnInvite = itemView.findViewById(R.id.btnInvite);

        }
    }
}

/*
package com.ygaps.travelapp.Network;

import com.ygaps.travelapp.Model.StopPointAPI;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.Model.ListTourAPI;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

*/
/**
 * Created by tpl on 12/13/16.
 *//*


public interface UserService {
    @POST("/user/login")
    //@POST("/login.php")
    Call<JsonObject> login(@Body JsonObject request);

    @POST("/logout.php")
    Call<Void> logout();

    @POST("/user/register")
    Call<JsonObject> register(@Body JsonObject request);

    @GET("/tour/list")
    Call<ListTourAPI> getListTours(@Header("Authorization") String TOKEN, @Query("rowPerPage") Number rowPerPage, @Query("pageNum") Number pageNum);
    @POST("/tour/create")
    Call<Tour> createTour(@Header("Authorization") String TOKEN, @Body Tour newTour);
    @POST("/tour/set-stop-points")
    Call<StopPointAPI> createStopPoint(@Header("Authorization") String TOKEN, @Body StopPointAPI stopPoint);
}*/
package com.ygaps.travelapp.Network;

import com.google.gson.JsonArray;
import com.ygaps.travelapp.Model.CommentsAPI;
import com.ygaps.travelapp.Model.FeedbackAPI;
import com.ygaps.travelapp.Model.ListTourAPI;
import com.ygaps.travelapp.Model.PointStarAPI;
import com.ygaps.travelapp.Model.ReviewsAPI;
import com.ygaps.travelapp.Model.StopPointAPI;
import com.ygaps.travelapp.Model.Tour;
import com.google.gson.JsonObject;
import com.ygaps.travelapp.Model.UserInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by tpl on 12/13/16.
 */

public interface UserService {
    @POST("/user/login")
        //@POST("/login.php")
    Call<JsonObject> login(@Body JsonObject request);

    @POST("/user/login/by-facebook")
    Call<JsonObject> loginByFacebook(@Body String accessToken);
    
    @POST("/logout.php")
    Call<Void> logout();

    @POST("/user/update-password")
    Call<JsonObject> updatePassword(@Header("Authorization") String TOKEN,@Body JsonObject request);

    @POST("/user/register")
    Call<JsonObject> register(@Body JsonObject request);

    @GET("/tour/list")
    Call<ListTourAPI> getListTours(@Header("Authorization") String TOKEN, @Query("rowPerPage") Number rowPerPage, @Query("pageNum") Number pageNum);

    @GET("/tour/get/review-point-stats")
    Call<PointStarAPI> getReviewPointStar(@Header("Authorization") String TOKEN, @Query("tourId") Number tourId);

    @GET("/tour/get/review-list")
    Call<ReviewsAPI> getListReview(@Header("Authorization") String TOKEN, @Query("tourId") Number tourId, @Query("rowPerPage") Number rowPerPage, @Query("pageNum") Number pageNum);

    @GET("/tour/history-user")
    Call<ListTourAPI> getHistoryTourOfUser(@Header("Authorization") String TOKEN, @Query("pageSize") Number rowPerPage, @Query("pageIndex") Number pageNum);

    @POST("/tour/create")
    Call<Tour> createTour(@Header("Authorization") String TOKEN, @Body Tour newTour);

    @POST("/tour/suggested-destination-list")
    Call<JsonObject> getSuggestDestinations(@Header("Authorization") String TOKEN, @Body JsonObject coordList);

    @POST("/tour/set-stop-points")
    Call<StopPointAPI> createStopPoint(@Header("Authorization") String TOKEN, @Body StopPointAPI stopPoint);

    @POST("/user/request-otp-recovery")
    Call<JsonObject> passwordRecover(@Body JsonObject request);

    @POST("/user/verify-otp-recovery")
    Call<JsonObject> verifyOTPRecovery(@Body JsonObject request);

    @GET("/tour/get/review-list")
    Call<ReviewsAPI> getReviews(@Header("Authorization") String TOKEN, @Query("tourId") Number tourId, @Query("pageIndex") Number pageIndex, @Query("pageSize") Number pageSize);

    @GET("/tour/info")
    Call<Tour> getTourDetails(@Header("Authorization") String TOKEN, @Query("tourId") Number tourId);

    @GET("/tour/comment-list")
    Call<CommentsAPI> getCommentsList(@Header("Authorization") String TOKEN, @Query("tourId") Long tourId, @Query("pageIndex") Integer pageIndex, @Query("pageSize") Integer pageSize);

    @GET("/tour/get/feedback-service")
    Call<FeedbackAPI> getStopPointComments(@Header("Authorization") String TOKEN, @Query("serviceId") Number serviceId, @Query("pageIndex") Integer pageIndex, @Query("pageSize") Integer pageSize);

    @POST("/tour/add/review")
    Call<JsonObject> addReview(@Header("Authorization") String TOKEN, @Body JsonObject request);

    @POST("/tour/add/feedback-service")
    Call<JsonObject> addPointReview(@Header("Authorization") String TOKEN, @Body JsonObject request);

    @GET("/tour/get/feedback-point-stats")
    Call<PointStarAPI> getFeedbackPointStar(@Header("Authorization") String TOKEN, @Query("serviceId") Number serviceId);

    @GET("/tour/search-history-user")
    Call<ListTourAPI> searchHistoryTour(@Header("Authorization") String TOKEN, @Query("searchKey") String searchKey, @Query("pageIndex") Number pageIndex, @Query("pageSize") Number pageSize);

    @POST("/user/notification/put-token")
    Call<JsonObject> registerFirebaseToken(@Header("Authorization") String TOKEN, @Body JsonObject request);

    @POST("/tour/current-users-coordinate")
    Call<JsonArray> getCoordinateOfMembers(@Header("Authorization") String TOKEN, @Body JsonObject request);

    @GET("/user/info")
    Call<UserInfo> getUserInfo(@Header("Authorization") String TOKEN);

    @GET("/tour/info")
    Call<JsonObject> getTourInfo(@Header("Authorization") String TOKEN, @Query("tourId") Number tourId);

    @POST("/user/edit-info")
    Call<UserInfo> updateUserInfo(@Header("Authorization") String TOKEN, @Body UserInfo userInfo);

    @GET("/user/search")
    Call<JsonObject> searchUser(@Query("searchKey") String searchKey, @Query("pageIndex") Number pageIndex, @Query("pageSize") Number pageSize);

    @POST("/tour/response/invitation")
    Call<JsonObject> responseToInvitation(@Header("Authorization") String TOKEN, @Body JsonObject request);

    @GET("/tour/get/invitation")
    Call<ListTourAPI> getListInvitation(@Header("Authorization") String TOKEN, @Query("pageIndex") Number pageIndex, @Query("pageSize") Number pageSize);

    @POST("tour/add/member")
    Call<JsonObject> addMember(@Header("Authorization") String TOKEN, @Body JsonObject request);
}


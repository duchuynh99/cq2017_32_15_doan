/*
package com.example.cq2017_32_15doan.Network;

import androidx.annotation.Nullable;

import com.example.cq2017_32_15doan.Manager.Constants;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyAPIClient {
    private static MyAPIClient instance;

    private Retrofit adapter;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;

    }


    @Nullable
    private String accessToken;

    private MyAPIClient() {
        OkHttpClient client = new OkHttpClient();
        adapter = new Retrofit.Builder()
                .baseUrl(Constants.APIEndpoint)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Retrofit getAdapter() {
        return adapter;
    }

    public static MyAPIClient getInstance() {
        if (instance == null)
            instance = new MyAPIClient();
        return instance;
    }
}
*/
package com.ygaps.travelapp.Network;

import androidx.annotation.Nullable;

import com.ygaps.travelapp.Manager.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyAPIClient {
    private static MyAPIClient instance;

    private Retrofit adapter;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;

    }

    @Nullable
    private String FCM;
    public  String getFCM()
    {
        return FCM;
    }
    public void setFCM(String FCM)
    {
        this.FCM = FCM;
    }

    @Nullable
    private String userId;
    public String getUserId()
    {
        return userId;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    @Nullable
    private String accessToken;

    private MyAPIClient() {

//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(logging);
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constants.APIEndpoint)
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(httpClient.build())
//                .build();

        OkHttpClient client = new OkHttpClient();
        adapter = new Retrofit.Builder()
                .baseUrl(Constants.APIEndpoint)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Retrofit getAdapter() {
        return adapter;
    }

    public static MyAPIClient getInstance() {
        if (instance == null)
            instance = new MyAPIClient();
        return instance;
    }
}

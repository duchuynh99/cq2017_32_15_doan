package com.ygaps.travelapp.Tour.ui.TravelAssistant;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ygaps.travelapp.Model.ListTourAPI;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.Tour.Activity.CreateTourActivity;
import com.ygaps.travelapp.Tour.Data.recyclerView.PaginationScrollListener;
import com.ygaps.travelapp.Tour.Data.recyclerView.ResponseAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TravelAssistantActivity extends AppCompatActivity {

    //Static code
    public static final int CREATE_TOUR_REQUEST_CODE = 2;

    //Network service
    String TOKEN = MyAPIClient.getInstance().getAccessToken();
    private UserService mService;

    //View
    private TextView mTrip;
    private LinearLayoutManager layoutManager;
    private ResponseAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private SearchView mSearchView;
    private FloatingActionButton mFAB;

    //Pagination RecyclerView
    private int rowPerPage = 10;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private int TOTAL_PAGES = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_travel_assistant);
        mTrip = findViewById(R.id.trip);
        mRecyclerView = findViewById(R.id.TourList);
        mSearchView = findViewById(R.id.searchAssistant);
        mFAB = findViewById(R.id.fab);


        mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);

//        mAdapter = new ResponseAdapter(this, new ArrayList<Tour>(0), new ResponseAdapter.PostItemListener() {
//            @Override
//            public void onPostClick(long id) {
//                Toast.makeText(TravelAssistantActivity.this, "Post id is " + id, Toast.LENGTH_SHORT).show();
//            }
//        });
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreTour();
                    }
                }, 1000);
            }

            @Override
            protected void removeFooterLastPage() {
                mAdapter.removeLoadingFooter();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public void show() {
                mFAB.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                mFAB.animate().translationY(mFAB.getHeight() + 1000).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });
        mRecyclerView.setHasFixedSize(true);
        initListTour();


        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateTourActivity.class);
                startActivityForResult(intent, CREATE_TOUR_REQUEST_CODE);
            }
        });
    }


    public void initListTour() {
        mService.getListTours(TOKEN, rowPerPage, currentPage).enqueue(new Callback<ListTourAPI>() {
            @Override
            public void onResponse(Call<ListTourAPI> call, Response<ListTourAPI> response) {
                if (response.isSuccessful()) {
                    String total = response.body().getTotal().toString();
                    TOTAL_PAGES = Integer.parseInt(total) / rowPerPage + 1;
                    String tripNumber = total + " trips";
                    mTrip.setText(tripNumber);
                    mAdapter.initListTour(new ArrayList<Tour>(response.body().getTours()));
                    if (currentPage <= TOTAL_PAGES)
                        mAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    Log.d("TravelAssistantActivity", "posts load from API");
                } else {
                    int statusCode = response.code();
                    //Xử lí lỗi tại đây
                    Log.d("TravelAssistantActivity", "Error Code" + statusCode);
                }
            }

            @Override
            public void onFailure(Call<ListTourAPI> call, Throwable t) {
                //Hiển thị lỗi.
                Log.d("TravelAssistantActivity", "error loading from API");
            }
        });
    }


    private void loadMoreTour() {
        Log.v("RECYCLERVIEW", "loadNextPage: " + currentPage);
        mService.getListTours(TOKEN, rowPerPage, currentPage).enqueue(new Callback<ListTourAPI>() {
            @Override
            public void onResponse(Call<ListTourAPI> call, Response<ListTourAPI> response) {
                if (response.isSuccessful()) {
                    mAdapter.removeLoadingFooter();
                    isLoading = false;
                    mAdapter.addAll(response.body().getTours());

                    if (currentPage != TOTAL_PAGES)
                        mAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    Log.d("TravelAssistantActivity", "posts load from API");
                } else {
                    int statusCode = response.code();
                    //Xử lí lỗi tại đây
                    Log.d("TravelAssistantActivity", "Error Code" + statusCode);
                }
            }

            @Override
            public void onFailure(Call<ListTourAPI> call, Throwable t) {
                //Hiển thị lỗi.
                Log.d("TravelAssistantActivity", "error loading from API");
            }
        });
    }

    //Search
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    /*MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_search, menu);
    MenuItem searchItem = menu.findItem(R.id.action_search);
    SearchView searchView = (SearchView) searchItem.getActionView();

    searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            mAdapter.getFilter().filter(newText);
            return false;
        }
    });*/
        return true;
    }
}

package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Model.Review;
import com.ygaps.travelapp.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {
    private Context mContext;
    private List<Review> mReviews;

    public ReviewsAdapter(Context mContext, List<Review> mReviews) {
        this.mContext = mContext;
        this.mReviews = mReviews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.review_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Review thisReview = mReviews.get(position);

        String url = "https://img.thuthuatphanmem.vn/uploads/2018/09/19/avatar-facebook-chat-1_105603552.gif";
        Picasso.get().load(url).fit().centerCrop().into(holder.ivUserAvatarReview);
        holder.tvUserNameReview.setText(thisReview.getName());
        holder.tvDateCreatedReview.setText(calendarFormatter(thisReview));
        holder.tvReview.setText(thisReview.getReview());
        holder.rbReviewRating.setRating(thisReview.getPoint());
    }

    @Override
    public int getItemCount() {
        return mReviews.size();
    }

    public void updateData(List<Review> reviews) {
        mReviews = reviews;
        notifyDataSetChanged();
    }

    //Data formatter-----------------------------------------------------------------------------------------------------
    private String calendarFormatter(Review review) {
        long start = Long.parseLong(review.getCreatedOn());
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(start);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(cal1.getTime());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivUserAvatarReview;
        TextView tvUserNameReview, tvDateCreatedReview, tvReview;
        RatingBar rbReviewRating;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivUserAvatarReview = itemView.findViewById(R.id.ivUserAvatarReview);
            tvUserNameReview = itemView.findViewById(R.id.tvUserNameReview);
            tvDateCreatedReview = itemView.findViewById(R.id.tvDateCreatedReview);
            rbReviewRating = itemView.findViewById(R.id.rbRatingReview);
            tvReview = itemView.findViewById(R.id.tvReview);
        }
    }
}
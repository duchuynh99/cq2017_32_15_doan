package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Model.Member;
import com.ygaps.travelapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MembersAdaptor extends RecyclerView.Adapter<MembersAdaptor.ViewHolder> {
    private ArrayList<Member> memberArrayList;
    private Context context;

    public MembersAdaptor(ArrayList<Member> memberArrayList, Context context) {
        this.memberArrayList = memberArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.member_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Member member = memberArrayList.get(position);
        String url = "https://img.thuthuatphanmem.vn/uploads/2018/09/19/avatar-facebook-chat-1_105603552.gif";
        Picasso.get().load(url).fit().centerCrop().into(holder.ivMemberAvatar);
        holder.tvMemberName.setText(member.getName());
        holder.tvMemberPhoneNumber.setText(member.getPhone());
        if (member.getIsHost() != null && member.getIsHost())
            holder.tvMemberHost.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return memberArrayList == null ? 0 : memberArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivMemberAvatar;
        TextView tvMemberName, tvMemberPhoneNumber, tvMemberHost;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivMemberAvatar = itemView.findViewById(R.id.ivMemberAvatar_TourDetails);
            tvMemberName = itemView.findViewById(R.id.tvMemberName_TourDetails);
            tvMemberPhoneNumber = itemView.findViewById(R.id.tvMemberPhoneNumber_TourDetails);
            tvMemberHost = itemView.findViewById(R.id.tvMemberHost_TourDetails);
        }
    }

    public void updateData(ArrayList<Member> memberList) {
        memberArrayList = memberList;
        notifyDataSetChanged();
    }
}
package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Model.Comment;
import com.ygaps.travelapp.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CommentsAdaptor extends RecyclerView.Adapter<CommentsAdaptor.ViewHolder> {

    private ArrayList<Comment> comments;
    private Context context;

    public CommentsAdaptor(ArrayList<Comment> comments, Context context) {
        this.comments = comments;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.comment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Comment comment = comments.get(position);
        String url = "https://img.thuthuatphanmem.vn/uploads/2018/09/19/avatar-facebook-chat-1_105603552.gif";
        Picasso.get().load(url).fit().centerCrop().into(holder.ivAvatar);
        holder.tvName.setText(comment.getName());
        holder.tvDateCreated.setText(calendarFormatter(comment));
        holder.tvComment.setText(comment.getComment());
    }

    @Override
    public int getItemCount() {
        return comments == null ? 0 : comments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivAvatar;
        TextView tvName, tvComment, tvDateCreated;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.ivCommentAvatar);
            tvName = itemView.findViewById(R.id.tvCommentName);
            tvDateCreated = itemView.findViewById(R.id.tvCommentDateCreated);
            tvComment = itemView.findViewById(R.id.tvComment);
        }

    }

    public void initData(List<Comment> comments) {
        this.comments = new ArrayList<>(comments);
        notifyDataSetChanged();
    }

    //Data formatter-----------------------------------------------------------------------------------------------------
    private String calendarFormatter(Comment comment) {
        Long start = comment.getCreatedOn();
        Calendar cal1 = Calendar.getInstance();
        cal1.setTimeInMillis(start);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(cal1.getTime());
    }
}

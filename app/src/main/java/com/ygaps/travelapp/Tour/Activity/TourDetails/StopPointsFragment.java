package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Dialog.StopPointInformation.StopPointInformationPopup;
import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.Tour.Data.recyclerView.StopPointAdapter;

import java.util.ArrayList;

public class StopPointsFragment extends Fragment {
    private ArrayList<StopPoint> stopPointArrayList;

    Context mContext;
    RecyclerView rvListStopPoints;
    StopPointAdapter mAdaptor;

    public StopPointsFragment(ArrayList<StopPoint> stopPointArrayList) {
        this.stopPointArrayList = stopPointArrayList;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stop_points_tour_details, container, false);
        addControls(view);

        rvListStopPoints.setLayoutManager(new LinearLayoutManager(mContext));
        mAdaptor = new StopPointAdapter(mContext, stopPointArrayList, new StopPointAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(StopPoint mStopPoint) {
                StopPointInformationPopup pointInformationPopup = new StopPointInformationPopup(mContext, mStopPoint);
                pointInformationPopup.show(getFragmentManager(), "Info");
            }
        });
        rvListStopPoints.setAdapter(mAdaptor);
        rvListStopPoints.setHasFixedSize(true);

        return view;
    }

    private void addControls(View view) {
        mContext = view.getContext();
        rvListStopPoints = view.findViewById(R.id.rvListStopPoint_TourDetails);
    }
}

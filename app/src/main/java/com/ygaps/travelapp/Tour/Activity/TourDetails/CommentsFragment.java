package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Model.Comment;
import com.ygaps.travelapp.Model.CommentsAPI;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsFragment extends Fragment {
    private Long tourId;
    Context context;
    RecyclerView rvListComments;
    CommentsAdaptor adaptor;

    String TOKEN = MyAPIClient.getInstance().getAccessToken();
//    String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoidGhpbmg5NyIsImVtYWlsIjoibmd1eWVubWluaHRoaW5oOTdAZ21haWwuY29tIiwiZXhwIjoxNTUzODczNDU1NTY2LCJpYXQiOjE1NTEyODE0NTV9.lQ-RkLSwD3UyRXWvSRaTIsn1f_3ZRMWd-nfRutcwXFw";

    public CommentsFragment(Long tourId) {
        this.tourId = tourId;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview_tour_details, container, false);
        addControls(view);
        rvListComments.setLayoutManager(new LinearLayoutManager(context));
        adaptor = new CommentsAdaptor(new ArrayList<Comment>(), context);
        rvListComments.setAdapter(adaptor);
        rvListComments.setHasFixedSize(true);
        fetchComments(tourId);
        return view;
    }

    private void fetchComments(Long tourId) {
        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.getCommentsList(TOKEN, tourId, 1, 225).enqueue(new Callback<CommentsAPI>() {
            @Override
            public void onResponse(Call<CommentsAPI> call, Response<CommentsAPI> response) {
                if (response.isSuccessful()) {
                    adaptor.initData(response.body().getCommentList());
                } else {
                    int statusCode = response.code();
                    //Xử lí lỗi tại đây
                    Log.d("TravelAssistantActivity", "Error Code" + statusCode);
                }
            }

            @Override
            public void onFailure(Call<CommentsAPI> call, Throwable t) {
                //Hiển thị lỗi.
                Log.d("TravelAssistantActivity", "error loading from API");
            }
        });
    }

    private void addControls(View view) {
        context = getContext();
        rvListComments = view.findViewById(R.id.rv_TourDetails);
    }
}

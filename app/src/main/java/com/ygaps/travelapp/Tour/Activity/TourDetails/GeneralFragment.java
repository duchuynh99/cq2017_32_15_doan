package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ygaps.travelapp.Model.PointStar;
import com.ygaps.travelapp.Model.PointStarAPI;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeneralFragment extends Fragment {
    private Tour thisTour;
    private Context mContext;

    private TextView tvTourName, tvCalendar, tvMembers, tvPrice;
    private ImageView ivStatus;
    private RatingBar rbEditRating, rbRatingAverage;
    private TextView tvRatingAverage;
    private ProgressBar pdReviewStar1, pdReviewStar2, pdReviewStar3, pdReviewStar4, pdReviewStar5;
    private EditText etTourReview;
    private Button btSubmitReview;

    String TOKEN = MyAPIClient.getInstance().getAccessToken();
//    String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoidGhpbmg5NyIsImVtYWlsIjoibmd1eWVubWluaHRoaW5oOTdAZ21haWwuY29tIiwiZXhwIjoxNTUzODczNDU1NTY2LCJpYXQiOjE1NTEyODE0NTV9.lQ-RkLSwD3UyRXWvSRaTIsn1f_3ZRMWd-nfRutcwXFw";

    public GeneralFragment(Tour thisTour) {
        this.thisTour = thisTour;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general_tour_details, container, false);
        addControls(view);

        if (thisTour.getName().isEmpty())
            tvTourName.setText(mContext.getString(R.string.null_tour_name).concat(" " + thisTour.getId()));
        else
            tvTourName.setText(thisTour.getName());
        tvCalendar.setText(thisTour.calendarFormatter());
        tvMembers.setText(thisTour.memberFormatter(mContext));
        tvPrice.setText(thisTour.priceRangeFormatter());
        loadStatus(view, thisTour.getStatus());
        loadRating(Long.parseLong(thisTour.getId()));
        btSubmitReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etTourReview.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, getString(R.string.review_submit_warning), Toast.LENGTH_SHORT).show();
                } else
                    submitTourReview();
            }
        });

        return view;
    }

    private void submitTourReview() {
        JsonObject body = new JsonObject();
        body.addProperty("tourId", thisTour.getId());
        body.addProperty("point", rbEditRating.getRating());
        body.addProperty("review", etTourReview.getText().toString());

        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.addReview(TOKEN, body).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.code() == 200) {
                    Toast.makeText(mContext, getString(R.string.review_submited), Toast.LENGTH_SHORT).show();
                    etTourReview.getText().clear();
                    etTourReview.clearFocus();
                } else
                    Toast.makeText(mContext, getString(R.string.review_submit_error).concat(" " + response.code()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(getContext(), getString(R.string.network_error), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void loadRating(Long id) {
        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.getReviewPointStar(TOKEN, id).enqueue(new Callback<PointStarAPI>() {
            @Override
            public void onResponse(Call<PointStarAPI> call, Response<PointStarAPI> response) {
                if (response.isSuccessful()) {
                    ArrayList<PointStar> mListPointStar = new ArrayList<>(response.body().getPointStats());
                    float averageRating = AverageRating(mListPointStar);
                    tvRatingAverage.setText(String.valueOf(averageRating));
                    rbRatingAverage.setRating(averageRating);
                    pdReviewStar1.setProgress(mListPointStar.get(0).getTotal());
                    pdReviewStar2.setProgress(mListPointStar.get(1).getTotal());
                    pdReviewStar3.setProgress(mListPointStar.get(2).getTotal());
                    pdReviewStar4.setProgress(mListPointStar.get(3).getTotal());
                    pdReviewStar5.setProgress(mListPointStar.get(4).getTotal());
                } else {
                    Log.d("API_Call", "[General Fragment] Error code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<PointStarAPI> call, Throwable t) {
                Log.d("API_Call", "[General Fragment] Can not connect to Server");
            }
        });
    }

    private void loadStatus(View view, Long status) {
        switch (Integer.parseInt(status.toString())) {
            case -1:
                ivStatus = view.findViewById(R.id.ivStatus_Canceled);
                ivStatus.setVisibility(View.VISIBLE);
                break;
            case 0:
                ivStatus = view.findViewById(R.id.ivStatus_Open);
                ivStatus.setVisibility(View.VISIBLE);
                break;
            case 1:
                ivStatus = view.findViewById(R.id.ivStatus_Start);
                ivStatus.setVisibility(View.VISIBLE);
                break;
            case 2:
                ivStatus = view.findViewById(R.id.ivStatus_Close);
                ivStatus.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void addControls(View view) {
        mContext = view.getContext();
        tvTourName = view.findViewById(R.id.tvName_TourDetails);
        tvCalendar = view.findViewById(R.id.tvCalendar_TourDetails);
        tvMembers = view.findViewById(R.id.tvMembers_TourDetails);
        tvPrice = view.findViewById(R.id.tvPrice_TourDetails);
        rbEditRating = view.findViewById(R.id.rbEditRating);
        tvRatingAverage = view.findViewById(R.id.tvRatingAverage);
        rbRatingAverage = view.findViewById(R.id.rbRatingAverage);
        pdReviewStar1 = view.findViewById(R.id.pdReviewStar1);
        pdReviewStar2 = view.findViewById(R.id.pdReviewStar2);
        pdReviewStar3 = view.findViewById(R.id.pdReviewStar3);
        pdReviewStar4 = view.findViewById(R.id.pdReviewStar4);
        pdReviewStar5 = view.findViewById(R.id.pdReviewStar5);
        etTourReview = view.findViewById(R.id.etTourReview);
        btSubmitReview = view.findViewById(R.id.btSubmitReview);
    }

    //Data calculator----------------------------------------------------------------------------------------------------
    private float AverageRating(ArrayList<PointStar> mListPointStar) {
        int sum = 0, numStart = 0;
        for (PointStar pointStar : mListPointStar) {
            sum += pointStar.getPoint() * pointStar.getTotal();
            numStart += pointStar.getTotal();
        }
        return numStart == 0 ? 0 : sum / numStart;
    }
}

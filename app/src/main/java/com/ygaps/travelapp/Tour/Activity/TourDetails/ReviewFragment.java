package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ygaps.travelapp.Model.Review;
import com.ygaps.travelapp.Model.ReviewsAPI;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewFragment extends Fragment {

    private Long TourId;
    private RecyclerView rvReviews;
    private ReviewsAdapter adapterReviews;
    private SwipeRefreshLayout srlRefresh;

    String TOKEN = MyAPIClient.getInstance().getAccessToken();
//    String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoidGhpbmg5NyIsImVtYWlsIjoibmd1eWVubWluaHRoaW5oOTdAZ21haWwuY29tIiwiZXhwIjoxNTUzODczNDU1NTY2LCJpYXQiOjE1NTEyODE0NTV9.lQ-RkLSwD3UyRXWvSRaTIsn1f_3ZRMWd-nfRutcwXFw";

    public ReviewFragment(Long tourId) {
        TourId = tourId;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review, container, false);
        addControl(view);
        srlRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchReviews(TourId);
            }
        });
        rvReviews.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterReviews = new ReviewsAdapter(getContext(), new ArrayList<Review>());
        rvReviews.setAdapter(adapterReviews);
        rvReviews.setHasFixedSize(true);
        fetchReviews(TourId);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        srlRefresh.setRefreshing(true);
    }

    private void fetchReviews(Long tourId) {
        UserService mUserService = MyAPIClient.getInstance().getAdapter().create(UserService.class);

        //Get review data from API
        mUserService.getReviews(TOKEN, tourId, 1, 225).enqueue(new Callback<ReviewsAPI>() {
            @Override
            public void onResponse(Call<ReviewsAPI> call, Response<ReviewsAPI> response) {
                if (response.isSuccessful()) {
                    srlRefresh.setRefreshing(false);
                    adapterReviews.updateData(response.body().getReviewList());
                } else {
                    Log.d("Reviews fragment", "Error Code " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ReviewsAPI> call, Throwable t) {
                //Hiển thị lỗi.
                Log.d("Reviews fragment", "error loading reviews from API");
            }
        });
    }

    //Helpers------------------------------------------------------------------------------------------------------------
    private void addControl(View view) {
        srlRefresh = view.findViewById(R.id.srlSwipeToRefress);
        rvReviews = view.findViewById(R.id.rvReviews);
    }
}

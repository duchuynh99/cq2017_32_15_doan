package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Model.Member;
import com.ygaps.travelapp.R;

import java.util.ArrayList;

public class MembersFragment extends Fragment {
    private ArrayList<Member> mListMembers;
    Context context;
    RecyclerView rvListMember;

    public MembersFragment(ArrayList<Member> mListMembers) {
        this.mListMembers = mListMembers;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview_tour_details, container, false);

        addControls(view);
        rvListMember.setLayoutManager(new LinearLayoutManager(context));
        rvListMember.setAdapter(new MembersAdaptor(mListMembers, context));
        rvListMember.setHasFixedSize(true);

        return view;
    }

    private void addControls(View view) {
        context = getContext();
        rvListMember = view.findViewById(R.id.rv_TourDetails);
    }
}
package com.ygaps.travelapp.Tour.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTourActivity extends AppCompatActivity {

    private EditText edtTourName, edtAdults, edtChildrens, edtMinCost, edtMaxCost;
    private Button btnStartDate, btnEndDate, btnCreateTour;
    private LinearLayout btnChooseTourImage;
    private CheckBox cbPrivateTour;
    private Tour newTour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_tour);
        //Set title & add back button
        addEventsActionBar();
        //ánh xạ các controls
        addControls();
        //Xử lí sự kiện
        addEvents();
    }

    private void addEventsActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.create_tour));
    }

    private void addControls() {
        edtTourName = findViewById(R.id.edtTourName);
        edtAdults = findViewById(R.id.edtAdults);
        edtChildrens = findViewById(R.id.edtChildrens);
        edtMaxCost = findViewById(R.id.edtMaxCost);
        edtMinCost = findViewById(R.id.edtMinCost);

        btnStartDate = findViewById(R.id.btnStartDate);
        btnEndDate = findViewById(R.id.btnEndDate);
        btnCreateTour = findViewById(R.id.btnCreateTour);

        btnChooseTourImage = findViewById(R.id.btnChooseTourImage);

        cbPrivateTour = findViewById(R.id.cbPrivateTour);
    }

    private void addEvents() {
        newTour = new Tour();
        //Xét sự kiện cho button Start date
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog datePickerDialog1 = new DatePickerDialog(CreateTourActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                btnStartDate.setText(date);
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                newTour.setStartDate(calendar.getTimeInMillis());
            }
        }, year, month, day);

        //Xét sự kiện cho button End date
        btnStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog1.show();
            }
        });
        final DatePickerDialog datePickerDialog2 = new DatePickerDialog(CreateTourActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                btnEndDate.setText(date);
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                newTour.setEndDate(calendar.getTimeInMillis());
            }
        }, year, month, day);
        btnEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog2.show();
            }
        });

        //Xét sự kiện thêm ảnh
        btnChooseTourImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //...
            }
        });
        //Create tour
        btnCreateTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //rỗng
                if (edtTourName.getText().toString().length() == 0)
                    edtTourName.setError(getResources().getString(R.string.error_empty));
                else if (btnStartDate.getText().toString().equals(getResources().getString(R.string.select_date)) //chưa chọn ngày
                        || btnEndDate.getText().toString().equals(getResources().getString(R.string.select_date))) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_date_tour), Toast.LENGTH_SHORT).show();
                }

                //xem xét thông tin có hợp lí không?
                else if (newTour.getEndDate() < newTour.getStartDate()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_date_tour), Toast.LENGTH_SHORT).show();
                } else {
                    //kiểm tra xong thêm thông tin cho newTour
                    newTour.setName(edtTourName.getText().toString());
                    newTour.setAdults(Integer.parseInt(edtAdults.getText().toString()));
                    newTour.setChilds(Integer.parseInt(edtChildrens.getText().toString()));
                    newTour.setMinCost(Float.parseFloat(edtMinCost.getText().toString()));
                    newTour.setMaxCost(Float.parseFloat(edtMaxCost.getText().toString()));
                    newTour.setPrivate(cbPrivateTour.isChecked());
                    String TOKEN = MyAPIClient.getInstance().getAccessToken();
                    //Tạo tour và gửi lên api:
                    UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
                    mService.createTour(TOKEN, newTour).enqueue(new Callback<Tour>() {
                        @Override
                        public void onResponse(Call<Tour> call, Response<Tour> response) {
                            if (response.code() == 200) {
                                String tourID = response.body().getId();
                                Toast.makeText(getApplicationContext(), getString(R.string.create_tour_success), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                                intent.putExtra(getString(R.string.key_transfer_id_tour), tourID);
                                startActivity(intent);
                                finish();
                            }
                            if (response.code() == 400) {
                                JsonParser parser = new JsonParser();
                                JsonObject error = parser.parse(response.errorBody().charStream()).getAsJsonObject();
                                JsonArray messageArray = error.get("message").getAsJsonArray();
                                edtTourName.setError(messageArray.get(0).getAsJsonObject().get("msg").getAsString());
                            }
                            if (response.code() == 500) {
                                JsonParser parser = new JsonParser();
                                JsonObject error = parser.parse(response.errorBody().charStream()).getAsJsonObject();
                                Toast.makeText(getApplicationContext(), error.get("message").getAsString(), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Tour> call, Throwable t) {

                        }
                    });
                    //Tiếp tục sang màn hình thêm điểm dừng & finish current activity

                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

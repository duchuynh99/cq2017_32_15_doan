package com.ygaps.travelapp.Tour.Data.recyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ygaps.travelapp.Model.Province;
import com.ygaps.travelapp.Model.ServiceType;
import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.R;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class StopPointAdapter extends RecyclerView.Adapter<StopPointAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<StopPoint> mListStopPoint;
    private OnItemClickListener mClickListener;
    private List<ServiceType> serviceList;
    private List<Province> provinceList;

    public StopPointAdapter(Context mContext, ArrayList<StopPoint> mListStopPoint, OnItemClickListener mClickListener) {
        this.mContext = mContext;
        this.mListStopPoint = mListStopPoint;
        this.mClickListener = mClickListener;
        this.serviceList = dataForListServiceType();
        this.provinceList = dataForListProvince();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        StopPointAdapter.ViewHolder viewHolder;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View itemView = layoutInflater.inflate(R.layout.stop_point_item, parent, false);
        viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        StopPoint currentStopPoint = mListStopPoint.get(position);
        holder.bind(currentStopPoint, mClickListener);
    }

    @Override
    public int getItemCount() {
        return mListStopPoint == null ? 0 : mListStopPoint.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivAvatar;
        TextView tvName, tvCalendar, tvProvince, tvService, tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAvatar = itemView.findViewById(R.id.ivAvatar_StopPoint);
            tvName = itemView.findViewById(R.id.tvStopPointName);
            tvCalendar = itemView.findViewById(R.id.tvCalendar_StopPoint);
            tvProvince = itemView.findViewById(R.id.tvProvince_StopPoint);
            tvService = itemView.findViewById(R.id.tvServiceType_StopPoint);
            tvPrice = itemView.findViewById(R.id.tvPrice_StopPoint);
        }

        public void bind(final StopPoint mStopPoint, final StopPointAdapter.OnItemClickListener listener) {
            String url = "http://dulichdanviet.com/image/data/moc-chau-mua-hoa-cai.jpg";
            Picasso.get().load(url).fit().centerCrop().into(ivAvatar);
            tvName.setText(mStopPoint.getName());
            tvCalendar.setText(mStopPoint.calendarFormatter());
            tvProvince.setText(provinceList.get(Integer.parseInt(mStopPoint.getProvinceId().toString())).getName());
            tvService.setText(serviceList.get(Integer.parseInt(mStopPoint.getServiceTypeId().toString())).getName());
            tvPrice.setText(mStopPoint.priceRangeFormatter());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(mStopPoint);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(StopPoint mStopPoint);
    }

    private List<ServiceType> dataForListServiceType() {
        ArrayList<ServiceType> listServiceType = new ArrayList<>();
        BufferedReader reader;
        String path_file = "service_type.csv";
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open(path_file)));
            String line = reader.readLine();
            int flag = 0;
            while (line != null) {
                if (flag > 0) {
                    String[] s = line.split(",");
                    listServiceType.add(new ServiceType(Integer.parseInt(s[0]), s[1]));
                }
                line = reader.readLine();
                flag++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listServiceType;
    }

    private List<Province> dataForListProvince() {
        ArrayList<Province> listProvince = new ArrayList<>();
        BufferedReader reader;
        String path_file = "province.csv";
        try {
            reader = new BufferedReader(new InputStreamReader(mContext.getAssets().open(path_file)));
            String line = reader.readLine();
            int flag = 0;
            while (line != null) {
                if (flag > 0) {
                    String[] s = line.split(",");
                    listProvince.add(new Province(Integer.parseInt(s[0]), s[1], s[2]));
                }
                line = reader.readLine();
                flag++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listProvince;
    }

    public void InitData(List<StopPoint> mListStopPoint) {
        this.mListStopPoint = new ArrayList<>(mListStopPoint);
        notifyDataSetChanged();
    }
}

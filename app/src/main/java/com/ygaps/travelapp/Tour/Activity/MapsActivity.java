package com.ygaps.travelapp.Tour.Activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.ygaps.travelapp.Dialog.ListStopPointPopup;
import com.ygaps.travelapp.Dialog.StopPointPopup;
import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.Model.StopPointAPI;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ygaps.travelapp.Manager.Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    public static final String TAG = "MapActivity";

    private GoogleMap mMap;
    private FloatingActionButton fabListStopPoint;
    private Button btnDone;
    private ArrayList<StopPoint> stopPointList = new ArrayList<>();
    private ArrayList<StopPoint> stopPointOnMapList = new ArrayList<>();
    private String tourID = "";
    private LocationManager mLocationManager;
    private UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
    private FusedLocationProviderClient fusedLocationClient;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        setContentView(R.layout.activity_maps);
        getTourID();
        initMap();

    }

    private void getTourID() {
        Intent intent = getIntent();
        tourID = intent.getStringExtra(getString(R.string.key_transfer_id_tour));
    }

    private void addControls() {
        fabListStopPoint = findViewById(R.id.fabListStopPoint);
        btnDone = findViewById(R.id.btnAddStopPointDone);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initMap() {

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        //ánh xạ các controls
        addControls();
        //xử lí tất cả sự kiện
        addEvents();


    }

    private void addEvents() {
        //Xử lí search địa điểm
        setupAutoCompleteFragment();

        final ListStopPointPopup listStopPointPopup = new ListStopPointPopup(getApplicationContext(), stopPointList);
        fabListStopPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listStopPointPopup.show(getSupportFragmentManager(), listStopPointPopup.getTag());

            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StopPointAPI stopPointAPI = new StopPointAPI(tourID, stopPointList);

                mService.createStopPoint(MyAPIClient.getInstance().getAccessToken(), stopPointAPI).enqueue(new Callback<StopPointAPI>() {
                    @Override
                    public void onResponse(Call<StopPointAPI> call, Response<StopPointAPI> response) {
                        if (response.code() == 200) {
                            Toast.makeText(getApplicationContext(), "Add stop point successfully!", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        if (response.code() == 400) {
                            JsonParser parser = new JsonParser();
                            JsonObject error = parser.parse(response.errorBody().charStream()).getAsJsonObject();
                            JsonArray messageArray = error.get("message").getAsJsonArray();
                            Toast.makeText(getApplicationContext(), messageArray.get(0).getAsJsonObject().get("msg").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                        if (response.code() == 500) {
                            JsonParser parser = new JsonParser();
                            JsonObject error = parser.parse(response.errorBody().charStream()).getAsJsonObject();
                            Toast.makeText(getApplicationContext(), error.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<StopPointAPI> call, Throwable t) {

                    }

                });
            }
        });

    }

    private void setupAutoCompleteFragment() {
        String apiKey = getString(R.string.key_google_map_api);
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }
        // Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);
        // Initialize the AutocompleteSupportFragment.

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
            }
        });
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                //Log.i(TAG, "An error occurred: " + status);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            mMap = googleMap;
            mMap.setMyLocationEnabled(true);
            LocationListener mLocationListener = new LocationListener() {
                @Override
                public void onLocationChanged(final Location location) {


                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    Activity#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for Activity#requestPermissions for more details.
                        return;
                    }
                    Location current = mLocationManager.getLastKnownLocation(provider);
                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                    0, mLocationListener);
            fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
                    //lấy điểm dừng gợi ý
                    JsonObject request = new JsonObject();
                    request.addProperty("hasOneCoordinate", true);
                    JsonObject coordList = new JsonObject();
                    coordList.addProperty("lat", location.getLatitude());
                    coordList.addProperty("long", location.getLongitude());
                    request.add("coordList", coordList);
                    mService.getSuggestDestinations(MyAPIClient.getInstance().getAccessToken(), request).enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.isSuccessful()) {
                                JsonArray stopPoints = response.body().getAsJsonArray("stopPoints");
                                for (int i = 0; i < stopPoints.size(); i++) {
                                    try {

                                        JsonObject obj = stopPoints.get(i).getAsJsonObject();
                                        StopPoint stopPoint = new StopPoint();
                                        stopPoint.setName(obj.get("name").getAsString());
                                        stopPoint.setAddress(obj.get("address").getAsString());
                                        stopPoint.setProvinceId(obj.get("provinceId").getAsNumber());
                                        stopPoint.setServiceTypeId(obj.get("serviceTypeId").getAsNumber());
                                        stopPoint.setLat(obj.get("lat").getAsDouble());
                                        stopPoint.setLong(obj.get("long").getAsDouble());
                                        stopPoint.setMinCost(obj.get("minCost").getAsInt());
                                        stopPoint.setMaxCost(obj.get("maxCost").getAsInt());
                                        int service_id = 0;
                                        if (stopPoint.getServiceTypeId() != null)
                                            service_id = stopPoint.getServiceTypeId().intValue();
                                        int icon = R.drawable.place;
                                        if (service_id == 1) {
                                            icon = R.drawable.food;
                                        } else if (service_id == 2) {
                                            icon = R.drawable.hotel;
                                        } else if (service_id == 3) {
                                            icon = R.drawable.bedtime;
                                        }
                                        LatLng position = new LatLng(stopPoint.get_lat().doubleValue(), stopPoint.get_long().doubleValue());
                                        MarkerOptions stopPointMarker = new MarkerOptions().position(position).title(stopPoint.getName()).snippet("").icon(BitmapDescriptorFactory.fromResource(icon));
                                        Marker marker = mMap.addMarker(stopPointMarker);
                                        stopPointOnMapList.add(stopPoint);
                                        marker.setTag(stopPointOnMapList.size() - 1);
                                        marker.showInfoWindow();

                                    } catch (Exception ex) {

                                    }
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }
            });





            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker) {
                    int position = (int) marker.getTag();
                    final StopPoint stopPoint = stopPointOnMapList.get(position);
                    StopPointPopup popup = new StopPointPopup(getApplicationContext(), stopPoint, tourID, null, stopPointList);
                    popup.show(getSupportFragmentManager(), popup.getTag());
                    getSupportFragmentManager().executePendingTransactions();
                    popup.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            int service_id = 0;
                            if (stopPoint.getServiceTypeId() != null)
                                service_id = stopPoint.getServiceTypeId().intValue();
                            int icon = R.drawable.place;
                            if (service_id == 1) {
                                icon = R.drawable.food;
                            } else if (service_id == 2) {
                                icon = R.drawable.hotel;
                            } else if (service_id == 3) {
                                icon = R.drawable.bedtime;
                            }
                            marker.setIcon(BitmapDescriptorFactory.fromResource(icon));

                        }
                    });
                    return true;
                }
            });
            mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                @Override
                public void onMapLongClick(final LatLng latLng) {
                    Geocoder geocoder = new Geocoder(getApplicationContext());

                    try {
                        List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 2);
                        final StopPoint stopPoint = new StopPoint();
                        stopPoint.setAddress(addresses.get(0).getAddressLine(0));
                        stopPoint.setLat(latLng.latitude);
                        stopPoint.setLong(latLng.longitude);
                        final StopPointPopup popup = new StopPointPopup(getApplicationContext(), stopPoint, tourID, addresses.get(0).getAdminArea(), stopPointList);
                        popup.show(getSupportFragmentManager(), popup.getTag());
                        getSupportFragmentManager().executePendingTransactions();
                        popup.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                int service_id = 0;
                                if (stopPoint.getServiceTypeId() != null)
                                    service_id = stopPoint.getServiceTypeId().intValue();
                                int icon = R.drawable.place;
                                if (service_id == 1) {
                                    icon = R.drawable.food;
                                } else if (service_id == 2) {
                                    icon = R.drawable.hotel;
                                } else if (service_id == 3) {
                                    icon = R.drawable.bedtime;
                                }
                                //stopPointList.add(stopPoint);
                                MarkerOptions stopPointMarker = new MarkerOptions().position(latLng).title(stopPoint.getName()).snippet("").icon(BitmapDescriptorFactory.fromResource(icon));
                                Marker marker = mMap.addMarker(stopPointMarker);
                                stopPointOnMapList.add(stopPoint);
                                marker.setTag(stopPointOnMapList.size() - 1);

                            }
                        });


                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            });

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initMap();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


}

package com.ygaps.travelapp.Tour.Data.recyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.R;

import java.util.ArrayList;
import java.util.List;

public class ResponseAdapter extends RecyclerView.Adapter<ResponseAdapter.ViewHolder> implements Filterable {
    //Pagination RecyclerView
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    //Click on item
    private final OnItemClickListener mClickListener;
    private boolean isLoadingAdded = false;
    //Tours
    private ArrayList<Tour> mTours;
    private ArrayList<Tour> mFilteredTours;
    private Context mContext;
    //Search
    private Filter localFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint == null || constraint.length() == 0) {
                mFilteredTours = mTours;
            } else {
                ArrayList<Tour> filteredList = new ArrayList<>();
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Tour tour : mTours) {
                    if ((tour.getName() != null && tour.getName().toLowerCase().contains(filterPattern))) {
                        filteredList.add(tour);
                    }
                }
                mFilteredTours = filteredList;
            }
            FilterResults results = new FilterResults();
            results.values = mFilteredTours;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilteredTours = (ArrayList<Tour>) results.values;
            notifyDataSetChanged();
        }
    };

    private Filter APIFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint == null || constraint.length() == 0) {
                mFilteredTours = mTours;
            } else {
                ArrayList<Tour> filteredList = new ArrayList<>();
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Tour tour : mTours) {
                    if ((tour.getName() != null && tour.getName().toLowerCase().contains(filterPattern)) || (tour.getId() != null && tour.getId().contains(filterPattern))) {
                        filteredList.add(tour);
                    }
                }
                mFilteredTours = filteredList;
            }
            FilterResults results = new FilterResults();
            results.values = mFilteredTours;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilteredTours.addAll((ArrayList<Tour>) results.values);
            notifyDataSetChanged();
        }
    };

    public ResponseAdapter(Context context, ArrayList<Tour> tours, OnItemClickListener clickListener) {
        mTours = tours;
        mContext = context;
        mClickListener = clickListener;
        mFilteredTours = tours;
    }

    @Override
    public ResponseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ResponseAdapter.ViewHolder viewHolder;
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (viewType) {
            case ITEM:
                View itemView = inflater.inflate(R.layout.tour_item, parent, false);
                viewHolder = new ViewHolder(itemView);
                break;
            case LOADING:
                View loadingView = inflater.inflate(R.layout.tour_load_more, parent, false);
                viewHolder = new ViewHolder(loadingView);
                break;
            default:
                viewHolder = null;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ResponseAdapter.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                Tour currentTour = mFilteredTours.get(position);
                holder.bind(currentTour, mClickListener);
                break;
            case LOADING:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredTours == null ? 0 : mFilteredTours.size();
    }

    public void initListTour(ArrayList<Tour> tours) {
        mTours = tours;
        mFilteredTours = tours;
        notifyDataSetChanged();
    }

    public void setListTour(ArrayList<Tour> tours) {
        mFilteredTours = tours;
        notifyDataSetChanged();
    }

    public void restoreListTour() {
        mFilteredTours = mTours;
        notifyDataSetChanged();
    }

    public void addMoreTour(List<Tour> moreTour) {
        for (Tour tour : moreTour) {
            mTours.add(tour);
            notifyItemInserted(mTours.size() - 1);
        }
    }

    private Tour getTour(int adapterPosition) {
        return mTours.get(adapterPosition);
    }

    public void searchTourAPI(String query) {

    }

    //Search----------------------------------------------------------------------------------------------------------
    @Override
    public Filter getFilter() {
        return localFilter;
    }

    //Helper----------------------------------------------------------------------------------------------------------
    @Override
    public int getItemViewType(int position) {
        return (position == mTours.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void add(Tour tour) {
        mTours.add(tour);
        notifyItemInserted(mTours.size() - 1);
    }

    public void addAll(List<Tour> moreTour) {
        for (Tour tour : moreTour) {
            add(tour);
        }
    }

    public void remove(Tour tour) {
        int position = mTours.indexOf(tour);
        if (position > -1) {
            mTours.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Tour());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mTours.size() - 1;
        Tour result = getItem(position);

        if (result != null) {
            mTours.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Tour getItem(int position) {
        return mTours.get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(Tour tour);

        void onInviteButtonClick(Tour tour);

        void onTrackButtonClick(Tour tour);

        void onDeleteButtonClick(Tour tour);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView Location, Calendar, Members, Price, IsHost;
        ImageButton ibPopupMenu;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.wallpaper);
            Location = itemView.findViewById(R.id.location);
            Calendar = itemView.findViewById(R.id.calendar);
            Members = itemView.findViewById(R.id.members);
            Price = itemView.findViewById(R.id.price);
            IsHost = itemView.findViewById(R.id.tvIsHost_Tour);
            ibPopupMenu = itemView.findViewById(R.id.ibPopupMenu);
        }

        public void bind(final Tour tour, final OnItemClickListener listener) {
            String url = "https://blog.royalcaribbeanbrasil.com.br/wp-content/uploads/2019/02/276013-veja-os-melhores-destinos-para-quem-quer-fugir-do-carnaval-e-descansar-1024x683.jpg";
            Picasso.get().load(url).fit().centerCrop().into(imageView);
            if (tour.getName() != null)
                Location.setText(tour.getName());
            Calendar.setText(tour.calendarFormatter());
            Members.setText(tour.memberFormatter(mContext));
            Price.setText(tour.priceRangeFormatter());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(tour);
                }
            });
            if (tour.getIsHost() != null && tour.getIsHost()) {
                IsHost.setVisibility(View.VISIBLE);
                ibPopupMenu.setVisibility(View.VISIBLE);
                ibPopupMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popupMenu = new PopupMenu(v.getContext(), v);
                        popupMenu.getMenuInflater().inflate(R.menu.menu_popup_recyclerview, popupMenu.getMenu());
                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.action_invite:
                                        mClickListener.onInviteButtonClick(tour);
                                        return true;
                                    case R.id.action_track:
                                        mClickListener.onTrackButtonClick(tour);
                                        return true;
                                    case R.id.action_delete:
                                        mClickListener.onDeleteButtonClick(tour);
                                        return true;
                                    default:
                                        return false;
                                }
                            }
                        });
                        popupMenu.show();
                    }
                });
            }
        }
    }
}

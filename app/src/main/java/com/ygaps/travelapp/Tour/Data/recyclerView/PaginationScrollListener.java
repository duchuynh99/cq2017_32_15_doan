package com.ygaps.travelapp.Tour.Data.recyclerView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {
    //Tải trang mới RecyclerView
    LinearLayoutManager layoutManager;

    //Tự động ẩn FAB
    static final float MINIMUM = 25;
    int scrollDistance = 0;
    boolean isVisible = true;

    public PaginationScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        //Tải trang mới RecyclerView
        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                loadMoreItems();
            }
        }
        if (!isLoading() && isLastPage()) {

        }

        //Tự động ẩn FAB
        if (isVisible && scrollDistance > MINIMUM) {//Ẩn đi nếu nó đang hiển thị và danh sách được kéo xuống
            hide();
            scrollDistance = 0;
            isVisible = false;
        } else if (!isVisible && scrollDistance < -MINIMUM) {//Hiện lại nếu nó đang ẩn và danh sách được kéo lên
            show();
            scrollDistance = 0;
            isVisible = true;
        }

        if ((isVisible && dy > 0) || (!isVisible && dy < 0)) {
            scrollDistance += dy;
        }

    }

    //Tải trang mới RecyclerView
    protected abstract void loadMoreItems();
    protected abstract void removeFooterLastPage();
    public abstract boolean isLastPage();
    public abstract boolean isLoading();

    //Tự động ẩn FAB
    public abstract void show();

    public abstract void hide();
}

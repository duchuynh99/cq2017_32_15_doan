package com.ygaps.travelapp.Tour.Activity.TourDetails;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.ygaps.travelapp.Dialog.StopPointInformation.CustomFragmentPagerAdapter;
import com.ygaps.travelapp.Model.Member;
import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TourDetailsActivity extends AppCompatActivity {
    Context mContext;
    ImageView ivAvatar;
    TextView tvTourName;
    TabLayout tabLayout;
    ViewPager viewPager;

    String TOKEN = MyAPIClient.getInstance().getAccessToken();
//    String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoidGhpbmg5NyIsImVtYWlsIjoibmd1eWVubWluaHRoaW5oOTdAZ21haWwuY29tIiwiZXhwIjoxNTUzODczNDU1NTY2LCJpYXQiOjE1NTEyODE0NTV9.lQ-RkLSwD3UyRXWvSRaTIsn1f_3ZRMWd-nfRutcwXFw";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_details);
        addControls();

        final Tour thisTour = (Tour) getIntent().getSerializableExtra("TourData");
        if (thisTour.getName().isEmpty())
            tvTourName.setText(mContext.getString(R.string.null_tour_name).concat(" " + thisTour.getId()));
        else
            tvTourName.setText(thisTour.getName());

        final CustomFragmentPagerAdapter adapter = new CustomFragmentPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(getString(R.string.tour_detail_general_tab_title), new GeneralFragment(thisTour));
        Long tourID = Long.parseLong(thisTour.getId());
        adapter.addFragment(getString(R.string.tour_detail_reviews_tab_title), new ReviewFragment(tourID));
//        adapter.addFragment(getString(R.string.tour_detail_comments_tab_title), new CommentsFragment(tourID));
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        fetchMoreTourDetails(adapter, tourID);
    }

    private void fetchMoreTourDetails(final CustomFragmentPagerAdapter adapter, Long id) {
        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.getTourDetails(TOKEN, id).enqueue(new Callback<Tour>() {
            @Override
            public void onResponse(Call<Tour> call, Response<Tour> response) {
                if (response.isSuccessful()) {
                    Tour tour = response.body();
                    ArrayList<Member> memberArrayList = new ArrayList<>(tour.getMembers());
                    ArrayList<StopPoint> stopPointArrayList = new ArrayList<>(tour.getStopPoints());
                    adapter.addFragment(getString(R.string.tour_detail_stopPoints_tab_title), new StopPointsFragment(stopPointArrayList));
                    adapter.addFragment(getString(R.string.tour_detail_members_tab_title), new MembersFragment(memberArrayList));
                    adapter.notifyDataSetChanged();
                } else {
                    int statusCode = response.code();
                    //Xử lí lỗi tại đây
                    Log.d("TourDetailsActivity", "Error Code" + statusCode);
                }
            }

            @Override
            public void onFailure(Call<Tour> call, Throwable t) {
                //Hiển thị lỗi.
                Log.d("TourDetailsActivity", "error loading from API");
            }
        });
    }

    private void addControls() {
        mContext = getBaseContext();
        ivAvatar = findViewById(R.id.ivAvatar_TourDetail);
        tvTourName = findViewById(R.id.tvTourName_TourDetails);
        String url = "https://blog.royalcaribbeanbrasil.com.br/wp-content/uploads/2019/02/276013-veja-os-melhores-destinos-para-quem-quer-fugir-do-carnaval-e-descansar-1024x683.jpg";
        Picasso.get().load(url).fit().centerCrop().into(ivAvatar);
        tabLayout = findViewById(R.id.tlTourDetails);
        viewPager = findViewById(R.id.vpTourDetails);
    }
}

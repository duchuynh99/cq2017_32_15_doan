package com.ygaps.travelapp.Register.ui.Register;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private EditText fullNameEditText ;
    private EditText emailEditText ;
    private EditText phoneEditText ;
    private EditText passwordEditText ;
    private EditText confirmPasswordEditText;
    private EditText addressEditText;
    private Button dobButton;
    private RadioGroup genderRadioButton;
    int gender = 1;
    private Button registerButton ;
    private ProgressBar loadingProgressBar;
    private UserService userService;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        fullNameEditText = findViewById(R.id.registerFullName);
        emailEditText = findViewById(R.id.registerEmail);
        phoneEditText = findViewById(R.id.registerPhone);
        passwordEditText = findViewById(R.id.registerPassword);
        confirmPasswordEditText = findViewById(R.id.registerConfirmPassword);
        registerButton = findViewById(R.id.registerRequest);
        addressEditText = findViewById(R.id.registerAddress);
        dobButton = findViewById(R.id.registerDob);
        genderRadioButton = findViewById(R.id.registerGenderGroup);

        genderRadioButton.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.male)
                {
                    gender = 1;
                }
                else
                if(checkedId == R.id.female)
                {
                    gender = 0;
                }
            }
        });
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog datePickerDialog1 = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                dobButton.setText(String.format("%04d/%02d/%02d", year, monthOfYear, dayOfMonth));
            }
        }, year, month, day);
        dobButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog1.show();
            }
        });
        loadingProgressBar = findViewById(R.id.loading);
        emailEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isEmailValid(emailEditText.getText().toString())) {
                    emailEditText.setError(getString(R.string.invalid_email));
                }
            }
        });
        phoneEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isPhoneValid(phoneEditText.getText().toString())) {
                    phoneEditText.setError(getString(R.string.invalid_phone));
                }
            }
        });
        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isPasswordValid(passwordEditText.getText().toString())) {
                    passwordEditText.setError(getString(R.string.invalid_password));
                }
            }
        });
        confirmPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isConfirmPasswordValid(confirmPasswordEditText.getText().toString(), passwordEditText.getText().toString())) {
                    confirmPasswordEditText.setError(getString(R.string.invalid_confirm_password));
                }
            }
        });
        confirmPasswordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    attemptRegister();
                    return true;
                }
                return false;
            }

        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegister();
            }
        });
    }

    private void attemptRegister() {
        try {
            if (isEmailValid(emailEditText.getText().toString()) && isPasswordValid(passwordEditText.getText().toString()) &&
                    isPhoneValid(phoneEditText.getText().toString()) && isConfirmPasswordValid(confirmPasswordEditText.getText().toString(), passwordEditText.getText().toString())) {
                loadingProgressBar.setVisibility(View.VISIBLE);

                final JsonObject request = new JsonObject();
                request.addProperty("email", emailEditText.getText().toString());
                request.addProperty("fullName", fullNameEditText.getText().toString());
                request.addProperty("phone", phoneEditText.getText().toString());
                request.addProperty("password", passwordEditText.getText().toString());
                request.addProperty("address", addressEditText.getText().toString());
                request.addProperty("dob", dobButton.getText().toString());
                request.addProperty("gender", gender);
                Call<JsonObject> call = userService.register(request);

                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.code() == 200) {
                            Intent intent = new Intent();
                            String email = response.body().get("email").getAsString();
                            intent.putExtra("status", getString(R.string.register_success));
                            intent.putExtra("email", email);
                            setResult(RESULT_OK, intent);
                            loadingProgressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(getApplicationContext(), getString(R.string.register_success), Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else if (response.code() == 400)
                        {
                            JsonParser parser = new JsonParser();
                            JsonObject error =  parser.parse(response.errorBody().charStream()).getAsJsonObject();
                            JsonArray messageArray = error.get("message").getAsJsonArray();
                            for (int i = 0; i < messageArray.size(); i++)
                            {
                                JsonObject current = messageArray.get(i).getAsJsonObject();
                                if(current.get("param").getAsString().equals("phone"))
                                {
                                    phoneEditText.setError(current.get("msg").getAsString());
                                }
                                if(current.get("param").getAsString().equals("email"))
                                {
                                    emailEditText.setError(current.get("msg").getAsString());
                                }
                            }
                            loadingProgressBar.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            JsonParser parser = new JsonParser();
                            JsonObject error =  parser.parse(response.errorBody().charStream()).getAsJsonObject();
                            String message = error.get("message").getAsString();
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }


                });
            }
        }
        catch (Exception ex)
        {
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_SHORT);
        }
    }

    private boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        }
        if (email.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        } else {
            return false;
        }
    }
    private boolean isPhoneValid(String phone) {
        if (phone == null) return false;
        if (phone.length() != 10) return false;
        return true;
    }
    private boolean isConfirmPasswordValid(String confirmPassword, String password) {
        return confirmPassword.equals(password);

    }
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 7;
    }
}
package com.ygaps.travelapp;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ygaps.travelapp.Model.UserInfo;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.Recorder.RecorderActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ygaps.travelapp.Manager.Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;

public class FollowTourMap extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String tourId;
    String userId;
    FloatingActionButton fabRecord;
    ArrayList<Marker> memberMarkerList = new ArrayList<Marker>();
    ArrayList<UserInfo> memberList = new ArrayList<>();
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_tour_map);
        fabRecord = findViewById(R.id.fabRecord);
        fabRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RecorderActivity.class);
                startActivity(intent);
            }
        });
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Intent intent = getIntent();
        tourId = intent.getStringExtra("tour_id");
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        userId = MyAPIClient.getInstance().getUserId();
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap = googleMap;
            mMap.setMyLocationEnabled(true);
            final UserService mUserService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
            LocationListener mLocationListener = new LocationListener() {
                @Override
                public void onLocationChanged(final Location location) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
                    JsonObject request = new JsonObject();
                    request.addProperty("tourId", tourId);
                    request.addProperty("userId", userId);
                    request.addProperty("lat", location.getLatitude());
                    request.addProperty("long", location.getLongitude());


                    Call<JsonArray> callGetCoordinateOfMembers = mUserService.getCoordinateOfMembers(MyAPIClient.getInstance().getAccessToken(), request);
                    callGetCoordinateOfMembers.enqueue(new Callback<JsonArray>() {
                        @Override
                        public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                            if (response.isSuccessful()) {
                                for(int i = 0;i < response.body().size();i++)
                                {
                                    JsonObject member = response.body().get(i).getAsJsonObject();
                                    if(member.get("id").getAsString().equals(userId))
                                    {
                                        continue;
                                    }
                                    LatLng memberLatLng = new LatLng(member.get("lat").getAsDouble(), member.get("long").getAsDouble());
                                    memberMarkerList.get(i).setPosition(memberLatLng);
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<JsonArray> call, Throwable t) {

                        }
                    });
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };
            LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000,
                    0, mLocationListener);



            Call<JsonObject> callGetTourInfo = mUserService.getTourInfo(MyAPIClient.getInstance().getAccessToken(), Integer.parseInt(tourId));
            callGetTourInfo.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> callGetTourInfo, Response<JsonObject> response) {
                    if (response.isSuccessful()) {

                        if (response.isSuccessful()) {
                            JsonArray members = response.body().getAsJsonArray("members");
                            for (int i = 0; i < members.size(); i++) {
                                JsonObject member = members.get(i).getAsJsonObject();
                                if(member.get("id").getAsInt() == Integer.parseInt(userId))
                                {
                                    continue;
                                }
                                MarkerOptions memberMarker = new MarkerOptions().position(new LatLng(0,0)).title(member.get("name").getAsString()).snippet("").icon(BitmapDescriptorFactory.fromResource(R.drawable.member));
                                Marker marker = mMap.addMarker(memberMarker);
                                marker.setTag(member.get("id").getAsString());
                                memberMarkerList.add(marker);
                            }
                            JsonArray stopPoints = response.body().getAsJsonArray("stopPoints");
                            for (int i = 0; i < stopPoints.size(); i++) {
                                JsonObject stopPoint = stopPoints.get(i).getAsJsonObject();
                                int service_id = 0;
                                if (stopPoint.get("serviceTypeId") != null)
                                    service_id = stopPoint.get("serviceTypeId").getAsInt();
                                int icon = R.drawable.place;
                                if (service_id == 1) {
                                    icon = R.drawable.food;
                                } else if (service_id == 2) {
                                    icon = R.drawable.hotel;
                                } else if (service_id == 3) {
                                    icon = R.drawable.bedtime;
                                }
                                MarkerOptions memberMarker = new MarkerOptions().position(new LatLng(stopPoint.get("lat").getAsDouble(), stopPoint.get("long").getAsDouble())).title(stopPoint.get("name").getAsString()).snippet("").icon(BitmapDescriptorFactory.fromResource(icon));
                                Marker marker = mMap.addMarker(memberMarker);
                                marker.setTag(stopPoint.get("id").getAsString());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> callGetTourInfo, Throwable t) {

                }
            });

            fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
                    JsonObject request = new JsonObject();
                    request.addProperty("tourId", tourId);
                    request.addProperty("userId", userId);
                    request.addProperty("lat", location.getLatitude());
                    request.addProperty("long", location.getLongitude());


                    Call<JsonArray> callGetCoordinateOfMembers = mUserService.getCoordinateOfMembers(MyAPIClient.getInstance().getAccessToken(), request);
                    callGetCoordinateOfMembers.enqueue(new Callback<JsonArray>() {
                        @Override
                        public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                            if (response.isSuccessful()) {
                                for(int i = 0;i < response.body().size();i++)
                                {
                                    JsonObject member = response.body().get(i).getAsJsonObject();
                                    if(member.get("id").getAsString().equals(userId))
                                    {
                                        continue;
                                    }
                                    LatLng memberLatLng = new LatLng(member.get("lat").getAsDouble(), member.get("long").getAsDouble());
                                    memberMarkerList.get(i).setPosition(memberLatLng);
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<JsonArray> call, Throwable t) {

                        }
                    });
                }
            });

        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onMapReady(mMap);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}

package com.ygaps.travelapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PointStarAPI {
    @SerializedName("pointStats")
    @Expose
    private List<PointStar> pointStats = null;

    public List<PointStar> getPointStats() {
        return pointStats;
    }

    public void setPointStats(List<PointStar> pointStats) {
        this.pointStats = pointStats;
    }
}
package com.ygaps.travelapp.Model;

public class ServiceType {
    int id;
    String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ServiceType(int id, String name) {
        this.id = id;
        this.name = name;
    }
}

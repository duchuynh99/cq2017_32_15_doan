package com.ygaps.travelapp.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class StopPointAPI implements Serializable {
    String tourId;
    ArrayList<StopPoint> stopPoints;
    ArrayList<Integer> deleteIds;

    public StopPointAPI(String tourId, ArrayList<StopPoint> stopPoints) {
        this.tourId = tourId;
        this.stopPoints = stopPoints;
    }

    public StopPointAPI(String tourId, ArrayList<StopPoint> stopPoints, ArrayList<Integer> deleteIds) {
        this.tourId = tourId;
        this.stopPoints = stopPoints;
        this.deleteIds = deleteIds;
    }

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public ArrayList<StopPoint> getStopPoints() {
        return stopPoints;
    }

    public void setStopPoints(ArrayList<StopPoint> stopPoints) {
        this.stopPoints = stopPoints;
    }

    public ArrayList<Integer> getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(ArrayList<Integer> deleteIds) {
        this.deleteIds = deleteIds;
    }
}

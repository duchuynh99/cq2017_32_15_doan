package com.ygaps.travelapp.Model;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ygaps.travelapp.R;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Tour implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("hostId")
    @Expose
    private String hostId;
    @SerializedName("hostName")
    @Expose
    private String hostName;
    @SerializedName("hostPhone")
    @Expose
    private String hostPhone;
    @SerializedName("hostEmail")
    @Expose
    private String hostEmail;
    @SerializedName("hostAvatar")
    @Expose
    private String hostAvatar;
    @SerializedName("status")
    @Expose
    private Long status;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("minCost")
    @Expose
    private Float minCost;
    @SerializedName("maxCost")
    @Expose
    private Float maxCost;
    @SerializedName("startDate")
    @Expose
    private Long startDate;
    @SerializedName("endDate")
    @Expose
    private Long endDate;
    @SerializedName("adults")
    @Expose
    private Integer adults;
    @SerializedName("childs")
    @Expose
    private Integer childs;
    @SerializedName("isPrivate")
    @Expose
    private Boolean isPrivate;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("stopPoints")
    @Expose
    private List<StopPoint> stopPoints = null;
    @SerializedName("comments")
    @Expose
    private List<Comment> comments = null;
    @SerializedName("members")
    @Expose
    private List<Member> members = null;
    @SerializedName("isHost")
    @Expose
    private Boolean isHost;
    @SerializedName("isKicked")
    @Expose
    private Boolean isKicked;
    @SerializedName("createdOn")
    @Expose
    private Long createdOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public Long getStatus() {
        return status;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getHostPhone() {
        return hostPhone;
    }

    public void setHostPhone(String hostPhone) {
        this.hostPhone = hostPhone;
    }

    public String getHostEmail() {
        return hostEmail;
    }

    public void setHostEmail(String hostEmail) {
        this.hostEmail = hostEmail;
    }

    public String getHostAvatar() {
        return hostAvatar;
    }

    public void setHostAvatar(String hostAvatar) {
        this.hostAvatar = hostAvatar;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getMinCost() {
        return minCost;
    }

    public void setMinCost(Float minCost) {
        this.minCost = minCost;
    }

    public Float getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(Float maxCost) {
        this.maxCost = maxCost;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public Integer getChilds() {
        return childs;
    }

    public void setChilds(Integer childs) {
        this.childs = childs;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<StopPoint> getStopPoints() {
        return stopPoints;
    }

    public void setStopPoints(List<StopPoint> stopPoints) {
        this.stopPoints = stopPoints;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public Boolean getIsHost() {
        return isHost;
    }

    public void setIsHost(Boolean isHost) {
        this.isHost = isHost;
    }

    public Boolean getIsKicked() {
        return isKicked;
    }

    public void setIsKicked(Boolean isKicked) {
        this.isKicked = isKicked;
    }


    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }


    //Data formatter;
    public String priceRangeFormatter() {
        String result = null;
        if (minCost != 0) {
            result = minCost.toString();
        }
        if (maxCost != 0) {
            if (result == null) {
                result = maxCost.toString() + " VNĐ";
            } else {
                result += " - " + maxCost.toString() + " VNĐ";
            }
        }
        if (result == null) {
            result = "FREE";
        }
        return result;
    }

    public String calendarFormatter() {
        String result = null;
        Long start, end;
        Calendar cal1, cal2;
        String pattern = "HH:mm  dd-MM-yyyy";//dd/MM/YYYY
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        start = startDate;
        end = endDate;
        cal1 = cal2 = Calendar.getInstance();

        if (start != null) {
            cal1.setTimeInMillis(start);
            result = formatter.format(cal1.getTime());
        }
        if (end != null && !(end.equals(start))) {
            cal2.setTimeInMillis(end);
            if (result == null) {
                result = formatter.format(cal2.getTime());
            } else {
                result += " - " + formatter.format(cal2.getTime());
            }
        }
        if (result == null) {
            result = "No calendar";
        }
        return result;
    }

    public String memberFormatter(Context mContext) {
        String result = null;
        if (adults.intValue() != 0) {
            result = adults.toString() + " " + mContext.getResources().getString(R.string.adults);
        }
        if (childs.intValue() != 0) {
            if (result == null) {
                result = childs.toString() + " " + mContext.getResources().getString(R.string.childs);
            } else {
                result += " - " + childs.toString() + " " + mContext.getResources().getString(R.string.childs);
            }
        }
        if (result == null) {
            result = "No member";
        }
        return result;
    }

    public String DateCreateFormatter() {
        if (createdOn != null) {
            String pattern = "HH:mm  dd-MM-yyyy";//dd/MM/YYYY
            SimpleDateFormat formatter = new SimpleDateFormat(pattern);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(createdOn);
            return formatter.format(calendar.getTime());
        }
        return "Unknown date created";
    }
}
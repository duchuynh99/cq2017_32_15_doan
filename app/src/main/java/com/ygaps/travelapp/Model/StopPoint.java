package com.ygaps.travelapp.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class StopPoint implements Serializable {
    Number id;
    String name, address;
    Number provinceId;
    @SerializedName("lat")
    Number _lat;
    @SerializedName("long")
    Number _long;
    Number arrivalAt, leaveAt, serviceTypeId;
    Number minCost, maxCost;
    String avatar;

    public StopPoint() {
    }

    public StopPoint(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public StopPoint(Number id, String name, String address, Number provinceId, Number _lat, Number _long, Number arrivalAt, Number leaveAt, Number serviceTypeId, Number minCost, Number maxCost, String avatar) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.provinceId = provinceId;
        this._lat = _lat;
        this._long = _long;
        this.arrivalAt = arrivalAt;
        this.leaveAt = leaveAt;
        this.serviceTypeId = serviceTypeId;
        this.minCost = minCost;
        this.maxCost = maxCost;
        this.avatar = avatar;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Number getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Number provinceId) {
        this.provinceId = provinceId;
    }

    public Number get_lat() {
        return _lat;
    }

    public void setLat(Number _lat) {
        this._lat = _lat;
    }

    public Number get_long() {
        return _long;
    }

    public void setLong(Number _long) {
        this._long = _long;
    }

    public Number getArrivalAt() {
        return arrivalAt;
    }

    public void setArrivalAt(Number arrivalAt) {
        this.arrivalAt = arrivalAt;
    }

    public Number getLeaveAt() {
        return leaveAt;
    }

    public void setLeaveAt(Number leaveAt) {
        this.leaveAt = leaveAt;
    }

    public Number getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Number serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public Number getMinCost() {
        return minCost;
    }

    public void setMinCost(Number minCost) {
        this.minCost = minCost;
    }

    public Number getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(Number maxCost) {
        this.maxCost = maxCost;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    //Data formatter;
    public String priceRangeFormatter() {
        String result = null;
        Integer minCost1 = Integer.parseInt(minCost.toString());
        Integer maxCost1 = Integer.parseInt(maxCost.toString());
        if (minCost1 != 0) {
            result = minCost1.toString();
        }
        if (maxCost1 != 0) {
            if (result == null) {
                result = maxCost1.toString() + " VNĐ";
            } else {
                result += " - " + maxCost1.toString() + " VNĐ";
            }
        }
        if (result == null) {
            result = "FREE";
        }
        return result;
    }

    public String calendarFormatter() {
        String result = null;
        Number start, end;
        Calendar cal1, cal2;
        String pattern = "HH:mm  dd-MM-yyyy";//dd/MM/YYYY
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        start = arrivalAt;
        end = leaveAt;
        cal1 = cal2 = Calendar.getInstance();

        if (start != null) {
            cal1.setTimeInMillis(Long.parseLong(start.toString()));
            result = formatter.format(cal1.getTime());
        }
        if (end != null && !(end.equals(start))) {
            cal2.setTimeInMillis(Long.parseLong(end.toString()));
            if (result == null) {
                result = formatter.format(cal2.getTime());
            } else {
                result += " - " + formatter.format(cal2.getTime());
            }
        }
        if (result == null) {
            result = "No calendar";
        }
        return result;
    }
}

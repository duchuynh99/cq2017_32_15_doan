package com.ygaps.travelapp.Main.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ygaps.travelapp.Model.ListTourAPI;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.Tour.Activity.CreateTourActivity;
import com.ygaps.travelapp.Tour.Activity.TourDetails.TourDetailsActivity;
import com.ygaps.travelapp.Tour.Data.recyclerView.PaginationScrollListener;
import com.ygaps.travelapp.Tour.Data.recyclerView.ResponseAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Assistant extends Fragment {
    //Static code
    public static final int TOUR_DETAILS_REQUEST_CODE = 2;
    public static final int CREATE_TOUR_REQUEST_CODE = 2;
    private static final int PAGE_START = 1;
    //Network service
    String TOKEN = MyAPIClient.getInstance().getAccessToken();
    //    String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoidGhpbmg5NyIsImVtYWlsIjoibmd1eWVubWluaHRoaW5oOTdAZ21haWwuY29tIiwiZXhwIjoxNTUzODczNDU1NTY2LCJpYXQiOjE1NTEyODE0NTV9.lQ-RkLSwD3UyRXWvSRaTIsn1f_3ZRMWd-nfRutcwXFw";
    private UserService mService;
    //View
    private Context mContext;
    private View view;
    private TextView mTrip;
    private LinearLayoutManager layoutManager;
    private ResponseAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout refreshLayout;
    private SearchView mSearchView;
    private FloatingActionButton mFAB;
    //Pagination RecyclerView
    private int rowPerPage = 10;
    private int currentPage = PAGE_START;
    private int TOTAL_PAGES = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    public Fragment_Assistant() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_travel_assistant, container, false);
        addControls();
        addEvents();
        return view;
    }

    private void addControls() {
        mSearchView = view.findViewById(R.id.searchAssistant);
        mTrip = view.findViewById(R.id.trip);
        mRecyclerView = view.findViewById(R.id.TourList);
        refreshLayout = view.findViewById(R.id.srlRefreshListTour);
        mSearchView = view.findViewById(R.id.searchAssistant);
        mFAB = view.findViewById(R.id.fab);
    }

    private void addEvents() {
        mContext = getContext();
        refreshLayout.setRefreshing(true);
        mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mAdapter = new ResponseAdapter(mContext, new ArrayList<Tour>(), new ResponseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Tour tour) {
//                Toast.makeText(mContext, "ID:" + tour.getAvatar(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, TourDetailsActivity.class);
                intent.putExtra("TourData", tour);
                startActivityForResult(intent, TOUR_DETAILS_REQUEST_CODE);
            }

            @Override
            public void onInviteButtonClick(Tour tour) {

            }

            @Override
            public void onTrackButtonClick(Tour tout) {

            }

            @Override
            public void onDeleteButtonClick(Tour tour) {

            }
        });
        layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadMoreTour();
                    }
                }, 1000);
            }

            @Override
            protected void removeFooterLastPage() {
                mAdapter.removeLoadingFooter();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

            @Override
            public void show() {
                mFAB.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                mFAB.animate().translationY(mFAB.getHeight() + 1000).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });
        mRecyclerView.setHasFixedSize(true);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = PAGE_START;
                TOTAL_PAGES = 0;
                isLoading = false;
                isLastPage = false;
                initListTour();
            }
        });
        initListTour();

        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CreateTourActivity.class);
                startActivityForResult(intent, CREATE_TOUR_REQUEST_CODE);
            }
        });
        addSearchView();
    }

    private void addSearchView() {
        mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setIconified(false);
            }
        });
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    public void initListTour() {
        mService.getListTours(TOKEN, rowPerPage, currentPage).enqueue(new Callback<ListTourAPI>() {
            @Override
            public void onResponse(Call<ListTourAPI> call, Response<ListTourAPI> response) {
                refreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    String total = response.body().getTotal().toString();
                    TOTAL_PAGES = Integer.parseInt(total) / rowPerPage + 1;
                    String tripNumber = total + " " + getString(R.string.trips_List_Tour_Activities);
                    mTrip.setText(tripNumber);
                    ArrayList<Tour> tourArrayList = new ArrayList<Tour>(response.body().getTours());
                    mAdapter.initListTour(tourArrayList);
                    if (currentPage < TOTAL_PAGES && tourArrayList.size() >= rowPerPage)
                        mAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    Log.d("TravelAssistantActivity", "posts load from API");
                } else {
                    int statusCode = response.code();
                    //Xử lí lỗi tại đây
                    Log.d("TravelAssistantActivity", "Error Code" + statusCode);
                }
            }

            @Override
            public void onFailure(Call<ListTourAPI> call, Throwable t) {
                //Hiển thị lỗi.
                Log.d("TravelAssistantActivity", "error loading from API");
            }
        });
    }

    private void loadMoreTour() {
        mService.getListTours(TOKEN, rowPerPage, currentPage).enqueue(new Callback<ListTourAPI>() {
            @Override
            public void onResponse(Call<ListTourAPI> call, Response<ListTourAPI> response) {
                if (response.isSuccessful()) {
                    mAdapter.removeLoadingFooter();
                    isLoading = false;
                    mAdapter.addAll(response.body().getTours());
                    if (currentPage < TOTAL_PAGES)
                        mAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    Log.d("TravelAssistantActivity", "posts load from API");
                } else {
                    int statusCode = response.code();
                    //Xử lí lỗi tại đây
                    Log.d("TravelAssistantActivity", "Error Code" + statusCode);
                    //Hiển thị lỗi.
                    Log.d("TravelAssistantActivity", "error loading from API");
                }
            }

            @Override
            public void onFailure(Call<ListTourAPI> call, Throwable t) {

            }
        });
    }
}




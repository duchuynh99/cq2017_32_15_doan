package com.ygaps.travelapp.Main.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.ygaps.travelapp.Dialog.StopPointInformation.StopPointInformationPopup;
import com.ygaps.travelapp.Login.ui.login.LoginActivity;
import com.ygaps.travelapp.Main.Activity.EditProfileActivity;
import com.ygaps.travelapp.Main.Activity.ResetPassActivity;
import com.ygaps.travelapp.Model.StopPoint;
import com.ygaps.travelapp.Model.UserInfo;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Setting extends Fragment {
    View view;
    TextView txtNameUser, txtEditProfile;
    Button btnSignOut;
    Spinner spinnerSwitchLanguage;
    ImageView imgAvatar;
    Button btnResetPassword;
    UserService userService;
    UserInfo userInfo = new UserInfo();
    public Fragment_Setting() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_setting, container, false);
        Button btnLogoutButton = view.findViewById(R.id.btnSignOut);
        btnLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyAPIClient.getInstance().setAccessToken("");
                Intent intent = new Intent(getActivity() ,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });
        addControls();
        addEvents();
        return view;
    }

    private void addEvents() {
        getUserInfoFromApi();
        initSpinner();
        addEventsSignOut();
        addEventResetPassword();
        //addEventEditProfile();
    }

    private void addEventResetPassword() {
        Intent intent = new Intent(getContext(), ResetPassActivity.class);
        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });
    }

    private void getUserInfoFromApi() {
        String token = MyAPIClient.getInstance().getAccessToken();
        userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        Call<UserInfo> call = userService.getUserInfo(token);
        call.enqueue(new Callback<UserInfo>(){

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if(response.code()==200){
                    userInfo = response.body();
                    MyAPIClient.getInstance().setUserId(String.format("%d", userInfo.getId()));
                    txtNameUser.setText(userInfo.getFull_name());
                    addEventEditProfile();
                    //Toast.makeText(getContext(),""+userInfo.getEmail()+" "+userInfo.getFull_name(),Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getContext(),"Get user info failed",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {

            }
        });
    }

    private void addEventEditProfile() {
        final Intent intent = new Intent(getContext(), EditProfileActivity.class);
        //Toast.makeText(getApplicationContext(),userInfo.getId(),Toast.LENGTH_LONG).show();

        txtEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra(getString(R.string.key_user_info),userInfo);
                startActivity(intent);
            }
        });
    }

    private void addEventsSignOut() {
        final Intent intent = new Intent(getContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyAPIClient.getInstance().setAccessToken("");
                SharedPreferences sharedPref = getContext().getSharedPreferences(getString(R.string.saved_access_token), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(getString(R.string.saved_access_token), "");
                editor.commit();
                getContext().startActivity(intent);
                getActivity().finish();
            }
        });
    }

    private void initSpinner() {
        String []list = getContext().getResources().getStringArray(R.array.language_array);
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSwitchLanguage.setAdapter(adapter);
        spinnerSwitchLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void addControls() {
        txtNameUser = view.findViewById(R.id.txtNameUser);
        txtEditProfile = view.findViewById(R.id.txtEditProfile);
        btnSignOut = view.findViewById(R.id.btnSignOut);
        spinnerSwitchLanguage = view.findViewById(R.id.spinnerSwitchLanguage);
        imgAvatar = view.findViewById(R.id.imgAvatar);
        btnResetPassword = view.findViewById(R.id.btnResetPassword);
    }

    @Override
    public void onResume() {
        getUserInfoFromApi();
        super.onResume();
    }
}

package com.ygaps.travelapp.Main.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ygaps.travelapp.Dialog.InvitationMemberPopup;
import com.ygaps.travelapp.FollowTourMap;
import com.ygaps.travelapp.Model.ListTourAPI;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.Tour.Activity.CreateTourActivity;
import com.ygaps.travelapp.Tour.Activity.TourDetails.TourDetailsActivity;
import com.ygaps.travelapp.Tour.Data.recyclerView.PaginationScrollListener;
import com.ygaps.travelapp.Tour.Data.recyclerView.ResponseAdapter;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ygaps.travelapp.Main.Fragment.Fragment_Assistant.TOUR_DETAILS_REQUEST_CODE;

public class Fragment_Tour extends Fragment {
    private View view;
    private Context mContext;

    //Static code
    public static final int CREATE_TOUR_REQUEST_CODE = 2;

    //Network service
    String TOKEN = MyAPIClient.getInstance().getAccessToken();
//    String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJuYW1lIjoidGhpbmg5NyIsImVtYWlsIjoibmd1eWVubWluaHRoaW5oOTdAZ21haWwuY29tIiwiZXhwIjoxNTUzODczNDU1NTY2LCJpYXQiOjE1NTEyODE0NTV9.lQ-RkLSwD3UyRXWvSRaTIsn1f_3ZRMWd-nfRutcwXFw";

    //View
    private TextView mTrip;
    private ResponseAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private SearchView mSearchView;
    private SwipeRefreshLayout refreshLayout;
    private FloatingActionButton mFAB;

    //Pagination RecyclerView
    private int rowPerPage = 10;
    private static final int PAGE_START = 1;
    private int currentPage = PAGE_START;
    private int TOTAL_PAGES = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    public Fragment_Tour() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_travel_assistant, container, false);
        addControls();
        addEvents();
        return view;
    }

    private void addControls() {
        mSearchView = view.findViewById(R.id.searchAssistant);
        mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setIconified(false);
            }
        });
        mTrip = view.findViewById(R.id.trip);
        mRecyclerView = view.findViewById(R.id.TourList);
        mSearchView = view.findViewById(R.id.searchAssistant);
        refreshLayout = view.findViewById(R.id.srlRefreshListTour);
        mFAB = view.findViewById(R.id.fab);
    }

    private void addEvents() {
        mContext = getContext();
        refreshLayout.setRefreshing(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new ResponseAdapter(mContext, new ArrayList<Tour>(0),
                new ResponseAdapter.OnItemClickListener() {

                    @Override
                    public void onItemClick(Tour tour) {
                        Intent intent = new Intent(mContext, TourDetailsActivity.class);
                        intent.putExtra("TourData", tour);
                        startActivityForResult(intent, TOUR_DETAILS_REQUEST_CODE);
                    }

                    @Override
                    public void onInviteButtonClick(Tour tour) {

                        InvitationMemberPopup invitationMemberPopup = new InvitationMemberPopup(getContext(),tour.getId());
                        invitationMemberPopup.show(getActivity().getSupportFragmentManager(),invitationMemberPopup.getTag());
                    }

                    @Override
                    public void onTrackButtonClick(Tour tour) {
                        Intent intent = new Intent(getContext(), FollowTourMap.class);
                        intent.putExtra("tour_id", tour.getId());
                        startActivity(intent);
                        Toast.makeText(mContext, tour.getId(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDeleteButtonClick(Tour tour) {

                    }
                });
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initListTour();
            }
        });
        initListTour();

        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CreateTourActivity.class);
                startActivityForResult(intent, CREATE_TOUR_REQUEST_CODE);
                //startActivity(intent);
            }
        });
        addSearchView();
    }

    private void addSearchView() {
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                refreshLayout.setRefreshing(false);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                refreshLayout.setRefreshing(true);
                searchTourAPI(newText);
                return false;
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                refreshLayout.setRefreshing(false);
                mAdapter.restoreListTour();
                return false;
            }
        });
    }

    private void searchTourAPI(String query) {
        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.searchHistoryTour(TOKEN, query, 1, 255).enqueue(new Callback<ListTourAPI>() {
            @Override
            public void onResponse(Call<ListTourAPI> call, Response<ListTourAPI> response) {
                refreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    mAdapter.setListTour(new ArrayList<>(response.body().getTours()));
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ListTourAPI> call, Throwable t) {

            }
        });
    }

    private void initListTour() {
        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.getHistoryTourOfUser(TOKEN, 255, 1).enqueue(new Callback<ListTourAPI>() {
            @Override
            public void onResponse(Call<ListTourAPI> call, Response<ListTourAPI> response) {
                refreshLayout.setRefreshing(false);
                ArrayList<Tour> tours = new ArrayList<>();
                for (Tour tour : response.body().getTours()) {
                    if (tour.getStatus() != -1)
                        tours.add(tour);
                }
                String tripNumber = tours.size() + " " + getString(R.string.trips_List_Tour_Activities);
                mTrip.setText(tripNumber);
                mAdapter.initListTour(tours);
            }

            @Override
            public void onFailure(Call<ListTourAPI> call, Throwable t) {
                //Hiển thị lỗi.
                Log.d("TravelAssistantActivity", "error loading from API");
            }
        });
    }

    private void loadMoreTour() {
        UserService mService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        mService.getHistoryTourOfUser(TOKEN, rowPerPage, currentPage).enqueue(new Callback<ListTourAPI>() {
            @Override
            public void onResponse(Call<ListTourAPI> call, Response<ListTourAPI> response) {
                if (response.isSuccessful()) {
                    mAdapter.removeLoadingFooter();
                    isLoading = false;
                    mAdapter.addAll(response.body().getTours());

                    if (currentPage != TOTAL_PAGES)
                        mAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    Log.d("TravelAssistantActivity", "posts load from API");
                } else {
                    int statusCode = response.code();
                    //Xử lí lỗi tại đây
                    Log.d("TravelAssistantActivity", "Error Code" + statusCode);
                    //Hiển thị lỗi.
                    Log.d("TravelAssistantActivity", "error loading from API");
                }
            }

            @Override
            public void onFailure(Call<ListTourAPI> call, Throwable t) {

            }
        });
    }
}


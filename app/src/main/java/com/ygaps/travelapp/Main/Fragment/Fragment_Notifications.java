package com.ygaps.travelapp.Main.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.JsonObject;
import com.ygaps.travelapp.FollowTourMap;
import com.ygaps.travelapp.Main.Adapter.NotificationsAdapter;
import com.ygaps.travelapp.Model.ListTourAPI;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Notifications extends Fragment {
    private Context context;
    private TextView tvTitle;
    private View view;
    private RecyclerView recyclerView;
    private NotificationsAdapter adapter;
    private SwipeRefreshLayout refreshLayout;
    private ArrayList<Tour> tourArrayList = new ArrayList<>();
    private String TOKEN = MyAPIClient.getInstance().getAccessToken();
//    String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiIxNDgiLCJwaG9uZSI6IjA5MTg1Mjg2NjQiLCJlbWFpbCI6ImtoYWNiYW8wMTJAZ21haWwuY29tIiwiZXhwIjoxNTgwMTkwMDA0MTA2LCJhY2NvdW50IjoidXNlciIsImlhdCI6MTU3NzU5ODAwNH0.kY3epIu__OUX52f8PDGNrGb19tCRmJqJ9FKDCSqbbgI";

    public Fragment_Notifications() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notifications, container, false);
        addControls(view);
        refreshLayout.setRefreshing(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new NotificationsAdapter(context, tourArrayList, new NotificationsAdapter.OnButtonClick() {
            @Override
            public void onAcceptButtonClick(Tour tour) {
                JsonObject request = new JsonObject();
                request.addProperty("tourId", tour.getId());
                request.addProperty("isAccepted", true);
                UserService userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
                Call<JsonObject> call = userService.responseToInvitation(MyAPIClient.getInstance().getAccessToken(), request);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if(response.isSuccessful())
                        {

                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onDeclineButtonClick(Tour tour) {
                JsonObject request = new JsonObject();
                request.addProperty("tourId", tour.getId());
                request.addProperty("isAccepted", false);
                UserService userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
                Call<JsonObject> call = userService.responseToInvitation(MyAPIClient.getInstance().getAccessToken(), request);
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if(response.isSuccessful())
                        {
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            }
        });
        recyclerView.setAdapter(adapter);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData(TOKEN, adapter);
            }
        });
        initData(TOKEN, adapter);
        return view;
    }

    private void initData(String token, NotificationsAdapter adapter) {
        UserService userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        userService.getListInvitation(TOKEN, 1, 255).enqueue(new Callback<ListTourAPI>() {
            @Override
            public void onResponse(Call<ListTourAPI> call, Response<ListTourAPI> response) {
                if (response.isSuccessful()) {
                    refreshLayout.setRefreshing(false);
                    String title = getString(R.string.you_have)
                            + " " + Integer.parseInt(response.body().getTotal().toString())
                            + " " + getString(R.string.notification);
                    tvTitle.setText(title);
                    adapter.InitData(response.body().getTours());
                } else {
                    Toast.makeText(context, "Error: " + response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ListTourAPI> call, Throwable t) {
                Toast.makeText(context, "Can't connect to API", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addControls(View view) {
        context = view.getContext();
        tvTitle = view.findViewById(R.id.notification_Number);
        recyclerView = view.findViewById(R.id.rvNotifications);
        refreshLayout = view.findViewById(R.id.srlRefresh_Notifications);
    }
}

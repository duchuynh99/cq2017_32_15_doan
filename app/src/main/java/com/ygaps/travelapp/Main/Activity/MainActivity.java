package com.ygaps.travelapp.Main.Activity;

import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.ygaps.travelapp.Main.Fragment.Fragment_Assistant;
import com.ygaps.travelapp.Main.Fragment.Fragment_Message;
import com.ygaps.travelapp.Main.Fragment.Fragment_Notifications;
import com.ygaps.travelapp.Main.Fragment.Fragment_Setting;
import com.ygaps.travelapp.Main.Fragment.Fragment_Tour;
import com.ygaps.travelapp.Main.Adapter.ViewPagerAdapter;
import com.ygaps.travelapp.Manager.MyApplication;
import com.ygaps.travelapp.Model.UserInfo;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.MyFirebaseService;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    ViewPager viewPager;
    MenuItem prevMenuItem;
    ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.nav_view);
        String token = MyAPIClient.getInstance().getAccessToken();
        UserService userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        Call<UserInfo> call = userService.getUserInfo(token);
        call.enqueue(new Callback<UserInfo>(){

            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if(response.code()==200){
                    UserInfo userInfo = response.body();
                    MyAPIClient.getInstance().setUserId(String.format("%d", userInfo.getId()));

                    //Toast.makeText(getContext(),""+userInfo.getEmail()+" "+userInfo.getFull_name(),Toast.LENGTH_LONG).show();
                }
                else{
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {

            }
        });
        addControls();
        setUpViewPager();
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        MyFirebaseService.getInstance().onNewToken(token);
                        // Log and toast

                    }
                });


    }

    private void addControls() {
        //Hide action bar
        getSupportActionBar().hide();

    }
    private void setUpViewPager(){

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.navigation_assistant:
                        viewPager.setCurrentItem(0);
                        testChecked(0);
                        break;
                    case R.id.navigation_tour:
                        viewPager.setCurrentItem(1);
                        testChecked(1);
                        break;
                    case R.id.navigation_message:
                        viewPager.setCurrentItem(2);
                        testChecked(2);
                        break;
                    case R.id.navigation_notifications:
                        viewPager.setCurrentItem(3);
                        testChecked(3);
                        break;
                    case R.id.navigation_setting:
                        viewPager.setCurrentItem(4);
                        testChecked(4);
                        break;
                }
                return false;
            }
        });
        viewPager = findViewById(R.id.viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if(prevMenuItem!=null){
                    prevMenuItem.setChecked(false);
                }
                else{
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                //Log.d("page","onPageSelected: "+i);
                bottomNavigationView.getMenu().getItem(i).setChecked(true);
                prevMenuItem = bottomNavigationView.getMenu().getItem(i);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        setupViewPager(viewPager);
        viewPager.setCurrentItem(0);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //finish();
    }
    private void setupViewPager(final ViewPager viewPager){
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new Fragment_Assistant());
        adapter.addFragment(new Fragment_Tour());
        adapter.addFragment(new Fragment_Message());
        adapter.addFragment(new Fragment_Notifications());
        adapter.addFragment(new Fragment_Setting());

        viewPager.setAdapter(adapter);


    }
    private void testChecked(int i) // poisition checked
    {

        bottomNavigationView.getMenu().getItem(0).setChecked(false);
        bottomNavigationView.getMenu().getItem(1).setChecked(false);
        bottomNavigationView.getMenu().getItem(2).setChecked(false);
        bottomNavigationView.getMenu().getItem(3).setChecked(false);
        bottomNavigationView.getMenu().getItem(4).setChecked(false);
        bottomNavigationView.getMenu().getItem(i).setChecked(true);


    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

package com.ygaps.travelapp.Main.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ygaps.travelapp.Model.UserInfo;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    ImageButton imbBack, imbSave;
    EditText edtFullname, edtEmail, edtPhone, edtAddress;
    Button btnDob;
    RadioGroup radGenderGroup;
    UserInfo userInfo = new UserInfo();
    UserService userService;
    ProgressDialog dialogLoading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        addControls();
        addEvents();
    }
    //Hàm sự kiện chính của màn hình
    private void addEvents() {
        //hide action bar
        getSupportActionBar().hide();
        //Get data from previous activity
        getUserInfo();
        //set data for edit text
        setDataForControls();
        //cancel and exit
        imbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //save data, send api, return data to previous activity
        final String token = MyAPIClient.getInstance().getAccessToken();
        userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        radGenderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i==R.id.male)
                    userInfo.setGender(1);
                else
                    userInfo.setGender(0);
            }
        });
        //Sự kiện lưu
        imbSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //save
                userInfo.setFull_name(edtFullname.getText().toString());
                userInfo.setPhone(edtPhone.getText().toString());
                userInfo.setAddress(edtAddress.getText().toString());
                final Call<UserInfo> call = userService.updateUserInfo(token,userInfo);
                call.enqueue(new Callback<UserInfo>() {
                    @Override
                    public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                        if(response.code()== 200){
                            //Thành công
                            Toast.makeText(getApplicationContext(),"Updated.",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Update failed.",Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInfo> call, Throwable t) {

                    }
                });
            }
        });
    }
    //Set data cho các controls
    private void setDataForControls() {
        edtFullname.setText(""+userInfo.getFull_name());
        edtEmail.setText(""+userInfo.getEmail());
        edtAddress.setText(userInfo.getAddress());
        edtPhone.setText(""+userInfo.getPhone());
        if(userInfo.getDob()!=null){
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = dateFormat.format(userInfo.getDob());
            btnDob.setText(strDate);
        }
        addEventDate(btnDob);
        radGenderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i==1)
                    radioGroup.check(R.id.male);
                else
                    radioGroup.check(R.id.female);

            }
        });
    }
    //Lấy dữ liệu từ previous activity
    private void getUserInfo() {
        Intent intent = getIntent();
        userInfo = (UserInfo) intent.getSerializableExtra(getString(R.string.key_user_info));
    }
    //Ánh xạ controls
    private void addControls() {
        imbBack = findViewById(R.id.imbBack);
        imbSave = findViewById(R.id.imbSave);
        edtFullname = findViewById(R.id.edtFullName);
        edtEmail = findViewById(R.id.edtEmail);
        edtPhone = findViewById(R.id.edtPhone);
        btnDob = findViewById(R.id.btnDob);
        radGenderGroup = findViewById(R.id.radGenderGroup);
        edtAddress= findViewById(R.id.edtAddress);
    }
    //Thêm sự kiện click button birthday
    private void addEventDate(final Button button) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog datePickerDialog = new DatePickerDialog(EditProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(year,monthOfYear,dayOfMonth);
                userInfo.setDob(calendar1.getTime());
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                button.setText(dateFormat.format(calendar1.getTime()));
            }
        }, 1990, month, day);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();

            }
        });
    }
}

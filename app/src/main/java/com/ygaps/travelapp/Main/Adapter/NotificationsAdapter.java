package com.ygaps.travelapp.Main.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.ygaps.travelapp.Model.Tour;
import com.ygaps.travelapp.R;

import java.util.ArrayList;
import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Tour> tours;
    private OnButtonClick buttonClickListener;

    public NotificationsAdapter(Context mContext, ArrayList<Tour> tours, OnButtonClick buttonClickListener) {
        this.mContext = mContext;
        this.tours = tours;
        this.buttonClickListener = buttonClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.invitation_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Tour tour = tours.get(position);
        holder.bind(tour, buttonClickListener);
    }

    @Override
    public int getItemCount() {
        return tours == null ? 0 : tours.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, status_open, status_close, status_start, status_cancel;
        TextView location, calendar, members, price, createOn, inviterName, inviterPhone;
        Button btDecline, btAccept;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ivAvatar_Invitation);
            location = itemView.findViewById(R.id.tvTourName_Invitation);
            calendar = itemView.findViewById(R.id.tvCalendar_Invitation);
            status_open = itemView.findViewById(R.id.ivStatus_Open_Invitation);
            status_cancel = itemView.findViewById(R.id.ivStatus_Canceled_Invitation);
            status_start = itemView.findViewById(R.id.ivStatus_Start_Invitation);
            status_close = itemView.findViewById(R.id.ivStatus_Close_Invitation);

            members = itemView.findViewById(R.id.tvMembers_Invitation);
            price = itemView.findViewById(R.id.tvPrice_Invitation);
            createOn = itemView.findViewById(R.id.tvDateCreated_Invitation);
            inviterName = itemView.findViewById(R.id.tvInviterName);
            inviterPhone = itemView.findViewById(R.id.tvInviterPhone);
            btDecline = itemView.findViewById(R.id.btDecline_Invitation);
            btAccept = itemView.findViewById(R.id.btAccept_Invitation);
        }

        public void bind(final Tour tour, final NotificationsAdapter.OnButtonClick listener) {
            String url = "https://blog.royalcaribbeanbrasil.com.br/wp-content/uploads/2019/02/276013-veja-os-melhores-destinos-para-quem-quer-fugir-do-carnaval-e-descansar-1024x683.jpg";
            Picasso.get().load(url).fit().centerCrop().into(imageView);
            if (tour.getName() != null) location.setText(tour.getName());
            calendar.setText(tour.calendarFormatter());
            members.setText(tour.memberFormatter(mContext));
            price.setText(tour.priceRangeFormatter());
            createOn.setText(tour.DateCreateFormatter());
            inviterName.setText(tour.getHostName());
            inviterPhone.setText(tour.getHostPhone());
            btAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onAcceptButtonClick(tour);
                    tours.remove(tour);
                    notifyDataSetChanged();
                }
            });
            btDecline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onDeclineButtonClick(tour);
                    tours.remove(tour);
                    notifyDataSetChanged();
                }
            });
            //Set status-----------------------------------------------------------------------------
            switch (Integer.parseInt(tour.getStatus().toString())) {
                case -1:
                    status_cancel.setVisibility(View.VISIBLE);
                    break;
                case 0:
                    status_open.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    status_start.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    status_close.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
    }

    public void InitData(List<Tour> tours) {
        this.tours = new ArrayList<>(tours);
        notifyDataSetChanged();
    }

    public interface OnButtonClick {
        void onAcceptButtonClick(Tour tour);

        void onDeclineButtonClick(Tour tour);
    }
}

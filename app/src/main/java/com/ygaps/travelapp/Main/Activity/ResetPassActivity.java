package com.ygaps.travelapp.Main.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassActivity extends AppCompatActivity {

    ImageButton imbBack, imbSave;
    EditText edtCurrentPass, edtNewPass, edtNewPassConfirm;
    UserService userService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);
        addControls();
        addEvents();
    }

    private void addEvents() {
        getSupportActionBar().hide();
        userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        imbBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        String token = MyAPIClient.getInstance().getAccessToken();
        JsonObject request = new JsonObject();

        imbSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtCurrentPass.getText().toString().length()==0){
                    edtCurrentPass.setError("Rỗng");
                }
                else if(!edtNewPass.getText().toString().equals(edtNewPassConfirm.getText().toString())){
                    edtNewPassConfirm.setError("Không trùng khớp");
                }
                else if(edtNewPass.getText().toString().equals(edtCurrentPass.getText().toString())){
                    edtNewPass.setError("Trùng mật khẩu hiện tại");

                }
                else{
                    request.addProperty("userId",MyAPIClient.getInstance().getUserId());
                    request.addProperty("currentPassword",edtCurrentPass.getText().toString());
                    request.addProperty("newPassword",edtNewPass.getText().toString());
                    Call<JsonObject> call = userService.updatePassword(token,request);
                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if(response.code()== 200){
                                finish();
                                Toast.makeText(getApplicationContext(),"Updated",Toast.LENGTH_SHORT).show();

                            }
                            else if(response.code()==400){
                                edtCurrentPass.setError("Sai mật khẩu");
                            }
                            else{
                                Toast.makeText(getApplicationContext(),"Sever error",Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }
            }
        });
    }



    private void addControls() {
        imbBack = findViewById(R.id.imbBack);
        imbSave = findViewById(R.id.imbSave);
        edtNewPass = findViewById(R.id.edtNewPass);
        edtCurrentPass = findViewById(R.id.edtCurrentPassword);
        edtNewPassConfirm = findViewById(R.id.edtNewPassConfirm);
    }
}

package com.ygaps.travelapp.Main.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.ygaps.travelapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_Message extends Fragment {
    View view;
    public Fragment_Message() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_message, container, false);

        return view;
    }


}

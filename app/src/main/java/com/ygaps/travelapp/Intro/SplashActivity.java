package com.ygaps.travelapp.Intro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;

import com.ygaps.travelapp.Login.ui.login.LoginActivity;
import com.ygaps.travelapp.Main.Activity.MainActivity;
import com.ygaps.travelapp.Manager.MyApplication;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.Network.MyAPIClient;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        MyApplication.getInstance().setAndroidId(android_id);
        Thread t=new Thread() {
            public void run() {

                try {
                    sleep(1500);

                    //start new activity
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.saved_access_token), MODE_PRIVATE);
                    String tokenStr = sharedPreferences.getString(getString(R.string.saved_access_token), "");
                    if(tokenStr != "")
                    {
                        MyAPIClient.getInstance().setAccessToken(tokenStr);
                        Intent intent = new Intent(SplashActivity.this , MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);

                    }
                    else
                    {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);

                    }

                    //destroying Splash activity


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

    }
}

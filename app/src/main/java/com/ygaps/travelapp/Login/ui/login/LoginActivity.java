package com.ygaps.travelapp.Login.ui.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.ygaps.travelapp.Main.Activity.MainActivity;
import com.ygaps.travelapp.Manager.Constants;
import com.ygaps.travelapp.Manager.MyApplication;
import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.MyFirebaseService;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.PasswordRecovery.PasswordRecovery;
import com.ygaps.travelapp.R;
import com.ygaps.travelapp.Register.ui.Register.RegisterActivity;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private ProgressBar mProgressView;
    private ProgressDialog mProgressDialog;
    private UserService userService;
    private Button loginButton, registerButton, recoveryButton;
    private LinearLayout loginFacebookButton, loginGoogleButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        mEmailView = (EditText) findViewById(R.id.username);
        mPasswordView = (EditText) findViewById(R.id.password);
        loginButton = (Button) findViewById(R.id.login);
        registerButton = findViewById(R.id.register);
        recoveryButton = findViewById(R.id.passwordRecoveryButton);
        recoveryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent passwordRecoveryIntent = new Intent(LoginActivity.this, PasswordRecovery.class);
                passwordRecoveryIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(passwordRecoveryIntent);
            }
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivityForResult(registerIntent, Constants.REGISTER_REQUEST_CODE);
            }
        });

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Đang xử lý...");
        mProgressDialog.setCancelable(false);
        userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        // Set up the login form.

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mProgressView = findViewById(R.id.loading);
        //eventLoginFacebook();
    }

/*
    private void eventLoginFacebook() {
        CallbackManager callbackManager;

        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email","public_profile");
        // If using in a fragment
        //loginButton.setFragment(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Call<JsonObject> call = userService.loginByFacebook(loginResult.getAccessToken().getToken());
                Log.e("token login by fb:",loginResult.getAccessToken().getToken());
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if (response.code() == 200) {
                            Toast.makeText(getApplicationContext(), String.format("Wellcome %s", mEmailView.getText().toString()), Toast.LENGTH_LONG).show();
                            MyAPIClient.getInstance().setAccessToken(response.body().get("token").getAsString());
                            long time = (new Date()).getTime() / 1000;
                            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.saved_access_token), MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.saved_access_token), response.body().get("token").getAsString());
                            editor.commit();

                            MyAPIClient.getInstance().setUserId(response.body().get("userId").getAsString());

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            LoginActivity.this.finish();
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                        }
                        else if(response.code()==400){
                            Toast.makeText(getApplicationContext(),"Đăng nhập bị lỗi!",Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(getApplicationContext(),"Tài khoản không tồn tại!",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });

            }

            @Override
            public void onCancel() {
                // App code
                callbackManager = CallbackManager.Factory.create();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }
*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }

    private void attemptLogin() {
        mProgressDialog.show();
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel == true) {
            mProgressDialog.hide();
        } else {
            final JsonObject request = new JsonObject();
            request.addProperty("emailPhone", mEmailView.getText().toString());
            request.addProperty("password", mPasswordView.getText().toString());
            Call<JsonObject> call = userService.login(request);

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    // Save login info
                    if (response.code() == 200) {
                        Toast.makeText(getApplicationContext(), String.format("Wellcome %s", mEmailView.getText().toString()), Toast.LENGTH_LONG).show();
                        MyAPIClient.getInstance().setAccessToken(response.body().get("token").getAsString());
                        long time = (new Date()).getTime() / 1000;
                        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.saved_access_token), MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(getString(R.string.saved_access_token), response.body().get("token").getAsString());
                        editor.commit();

                        MyAPIClient.getInstance().setUserId(response.body().get("userId").getAsString());

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        LoginActivity.this.finish();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                    if (response.code() == 400) {
                        JsonParser parser = new JsonParser();
                        JsonObject error = parser.parse(response.errorBody().charStream()).getAsJsonObject();
                        Toast.makeText(getApplicationContext(), error.get("message").getAsString(), Toast.LENGTH_LONG).show();
                    }
                    if (response.code() == 404) {
                        JsonParser parser = new JsonParser();
                        JsonObject error = parser.parse(response.errorBody().charStream()).getAsJsonObject();
                        Toast.makeText(getApplicationContext(), error.get("message").getAsString(), Toast.LENGTH_LONG).show();
                    }
                    mProgressDialog.hide();
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    //Log.d(TAG, t.getMessage());
                    Toast.makeText(getApplicationContext(), "Lỗi đăng nhập!", Toast.LENGTH_LONG).show();
                    mProgressDialog.hide();
                }
            });
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
//        return email.contains("@");
        return true;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }


    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }*/
}

package com.ygaps.travelapp.PasswordRecovery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordRecovery extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_recovery);
        final LinearLayout emailRecovery = findViewById(R.id.emailRecovery);
        emailRecovery.setSelected(true);
        final LinearLayout smsRecovery = findViewById(R.id.smsRecovery);
        smsRecovery.setSelected(false);
        final EditText edtEmailPhone = findViewById(R.id.recoveryEmailPhone);
        Button btnRecover = findViewById(R.id.btnRecover);
        emailRecovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailRecovery.setSelected(true);
                smsRecovery.setSelected(false);
                edtEmailPhone.setHint(R.string.email_recovery_hint);
            }
        });
        smsRecovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailRecovery.setSelected(false);
                smsRecovery.setSelected(true);
                edtEmailPhone.setHint(R.string.sms_recovery_hint);
            }
        });
        final UserService userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        btnRecover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailRecovery.isSelected())
                {
                    JsonObject request = new JsonObject();
                    request.addProperty("type", "email");
                    request.addProperty("value", edtEmailPhone.getText().toString());

                    Call<JsonObject> call = userService.passwordRecover(request);
                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if(response.code() == 200)
                            {
                                Intent intent = new Intent(PasswordRecovery.this, OTPVerification.class);
                                intent.putExtra("message", getString(R.string.email_recovery_message));
                                intent.putExtra("user_id", response.body().get("userId").getAsInt());
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                            else
                            {
                                JsonParser parser = new JsonParser();
                                JsonObject error =  parser.parse(response.errorBody().charStream()).getAsJsonObject();
                                String message = error.get("message").getAsString();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }
                if(smsRecovery.isSelected())
                {
                    JsonObject request = new JsonObject();
                    request.addProperty("type", "phone");
                    request.addProperty("value", edtEmailPhone.getText().toString());

                    Call<JsonObject> call = userService.passwordRecover(request);
                    call.enqueue(new Callback<JsonObject>() {
                        @Override
                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if(response.code() == 200)
                            {
                                Intent intent = new Intent(PasswordRecovery.this, OTPVerification.class);
                                intent.putExtra("message", getString(R.string.sms_recovery_hint));
                                intent.putExtra("user_id", response.body().get("userId").getAsInt());
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                            else
                            {
                                JsonParser parser = new JsonParser();
                                JsonObject error =  parser.parse(response.errorBody().charStream()).getAsJsonObject();
                                String message = error.get("message").getAsString();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<JsonObject> call, Throwable t) {

                        }
                    });
                }

            }
        });
    }
}

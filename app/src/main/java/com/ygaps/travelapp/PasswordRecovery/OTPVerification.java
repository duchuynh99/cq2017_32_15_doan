package com.ygaps.travelapp.PasswordRecovery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.UserService;
import com.ygaps.travelapp.R;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPVerification extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        Intent intent = getIntent();
        TextView message = findViewById(R.id.recoveryMessage);
        message.setText(intent.getStringExtra("message"));
        int userId = intent.getIntExtra("user_id", -1);
        final EditText OTP = findViewById(R.id.OTP);
        final EditText newPassword = findViewById(R.id.newPassword);
        final EditText confirmNewPassword = findViewById(R.id.confirmNewPassword);

        OTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isValidOTPCode(OTP.getText().toString())) {
                    OTP.setError(getString(R.string.invalid_otp));
                }
            }
        });
        newPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isPasswordValid(newPassword.getText().toString())) {
                    newPassword.setError(getString(R.string.invalid_password));
                }
            }
        });
        confirmNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isConfirmPasswordValid(confirmNewPassword.getText().toString(), newPassword.getText().toString())) {
                    confirmNewPassword.setError(getString(R.string.invalid_confirm_password));
                }
            }
        });
        Button submit = findViewById(R.id.submitNewPassword);
        UserService userService = MyAPIClient.getInstance().getAdapter().create(UserService.class);
        final JsonObject request = new JsonObject();
        request.addProperty("userId", userId);
        request.addProperty("newPassword", newPassword.getText().toString());
        request.addProperty("verifyCode", OTP.getText().toString());
        final Call<JsonObject> call = userService.verifyOTPRecovery(request);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        if(response.code() == 200)
                        {
                            Toast.makeText(getApplicationContext(), "Recovery successfully", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else
                        {
                            JsonParser parser = new JsonParser();
                            JsonObject error =  parser.parse(response.errorBody().charStream()).getAsJsonObject();
                            String message = error.get("message").getAsString();
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            }
        });

    }

    private boolean isValidOTPCode(String toString) {
        if(toString.length() != 6) return false;
        return true;
    }

    private boolean isConfirmPasswordValid(String confirmPassword, String password) {
        return confirmPassword.equals(password);

    }
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 7;
    }
}

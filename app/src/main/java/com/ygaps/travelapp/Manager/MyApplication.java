package com.ygaps.travelapp.Manager;

import android.app.Application;
import android.content.SharedPreferences;

import com.ygaps.travelapp.Network.MyAPIClient;
import com.ygaps.travelapp.Network.MyFirebaseService;
import com.ygaps.travelapp.R;

public class MyApplication extends Application {

    private String tokenInfo;
    private static MyApplication instance;
    public String getTokenInfo() {
        return tokenInfo;
    }
    public void setTokenInfo(String tokenInfo) {
        this.tokenInfo = tokenInfo;
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.saved_access_token), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.saved_access_token), tokenInfo);
        editor.commit();
    }

    public void loadTokenInfo() {
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.saved_access_token), MODE_PRIVATE);
        String tokenStr = sharedPref.getString(getString(R.string.saved_access_token), "");
        this.tokenInfo = tokenStr;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        loadTokenInfo();
    }
    private String androidId;

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getAndroidId() {
        return androidId;
    }

    public static MyApplication getInstance() {
        if (instance == null)
            instance = new MyApplication();
        return instance;
    }
}
